
package science.unlicense.project.build.core.dependency;

import science.unlicense.format.json.JSONUtilities;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;

/**
 *
 * @author Johann Sorel
 */
public class FolderDependencyResolver implements DependencyResolver {

    private static final Chars SUFFIX_META  = new Chars(".meta");
    private static final Chars GROUP_ID     = CommonPlugin.GROUP_ID;
    private static final Chars MODULE_ID    = CommonPlugin.MODULE_ID;
    private static final Chars VERSION      = CommonPlugin.VERSION;
    private static final Chars TYPE         = CommonPlugin.TYPE;
    private static final Chars DEPENDENCIES = CommonPlugin.DEPENDENCIES;

    private final Path base;

    public FolderDependencyResolver(Path base) {
        this.base = base;
    }

    @Override
    public Path find(ArtifactIdentifier dependency) throws IOException {
        final Chars artifactName = dependency.getArtifactName();

        Path artifactPath = getArtifactDirectory(dependency);
        artifactPath = artifactPath.resolve(artifactName);
        return artifactPath.exists() ? artifactPath : null;
    }

    @Override
    public Path store(ArtifactIdentifier dependency, Path artifact, ArtifactIdentifier[] transitive) throws IOException {

        //copy artifact in repository folders
        final Chars artifactName = dependency.getArtifactName();
        Path outPath = getArtifactDirectory(dependency);
        outPath.createContainer();
        outPath = outPath.resolve(artifactName);
        IOUtilities.copy(artifact.createInputStream(), outPath.createOutputStream());

        //store metadatas
        final Path metaPath = getMetaPath(dependency);
        final Document meta = new DefaultDocument();
        meta.setPropertyValue(GROUP_ID, dependency.groupId);
        meta.setPropertyValue(MODULE_ID, dependency.moduleId);
        meta.setPropertyValue(VERSION, dependency.version);
        meta.setPropertyValue(TYPE, dependency.type);

        if (transitive!=null && transitive.length!=0) {
            final Document[] trss = new Document[transitive.length];
            for (int i=0;i<transitive.length;i++) {
                trss[i] = new DefaultDocument();
                trss[i].setPropertyValue(GROUP_ID, transitive[i].groupId);
                trss[i].setPropertyValue(MODULE_ID, transitive[i].moduleId);
                trss[i].setPropertyValue(VERSION, transitive[i].version);
                trss[i].setPropertyValue(TYPE, transitive[i].type);
            }
            meta.setPropertyValue(DEPENDENCIES, trss);
        }
        JSONUtilities.writeDocument(meta, metaPath);

        return outPath;
    }

    private Path getArtifactDirectory(ArtifactIdentifier dependency) {
        Path dirPath = base;
        for (Chars part : dependency.groupId.split('.')) {
            dirPath = dirPath.resolve(part);
        }
        dirPath = dirPath.resolve(dependency.moduleId);
        dirPath = dirPath.resolve(dependency.version);
        return dirPath;
    }

    @Override
    public ArtifactIdentifier[] listTransitive(ArtifactIdentifier dependency) throws IOException {
        final Path metaPath = getMetaPath(dependency);
        if (!metaPath.exists()) {
            throw new IOException("Depdency not store in this repository");
        }
        final Document meta = JSONUtilities.readAsDocument(metaPath, null);
        return getDependencies(meta);
    }

    private Path getArtifactPath(ArtifactIdentifier dep) {
        final Path dir = getArtifactDirectory(dep);
        return dir.resolve(dep.getArtifactName());
    }

    private Path getMetaPath(ArtifactIdentifier dep) {
        final Path dir = getArtifactDirectory(dep);
        return dir.resolve(dep.getArtifactName().concat(SUFFIX_META));
    }

    protected static ArtifactIdentifier[] getDependencies(Document doc) {
        final Object val = doc.getPropertyValue(DEPENDENCIES);
        if (val instanceof Document) {
            final Document rd = (Document) val;
            final Chars group = (Chars) rd.getPropertyValue(GROUP_ID);
            final Chars module = (Chars) rd.getPropertyValue(MODULE_ID);
            final Chars version = (Chars) rd.getPropertyValue(VERSION);
            final Chars type = (Chars) rd.getPropertyValue(TYPE);
            return new ArtifactIdentifier[]{new ArtifactIdentifier(group, module, version, type)};
        } else if(val instanceof Object[]) {
            final Object[] array = (Object[]) val;
            final ArtifactIdentifier[] values = new ArtifactIdentifier[array.length];
            for (int i=0;i<array.length;i++) {
                final Document rd = (Document) array[i];
                final Chars group = (Chars) rd.getPropertyValue(GROUP_ID);
                final Chars module = (Chars) rd.getPropertyValue(MODULE_ID);
                final Chars version = (Chars) rd.getPropertyValue(VERSION);
                final Chars type = (Chars) rd.getPropertyValue(TYPE);
                values[i] = new ArtifactIdentifier(group, module, version, type);
            }
            return values;
        } else if (val == null) {
            return new ArtifactIdentifier[0];
        } else {
            throw new RuntimeException("Unexpected dependency value : "+val);
        }
    }

}
