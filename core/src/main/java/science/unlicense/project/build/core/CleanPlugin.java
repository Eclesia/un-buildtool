
package science.unlicense.project.build.core;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class CleanPlugin extends CommonPlugin {

    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        if (tgtPathStr != null) {
            Path targetPath = rootPath.resolve(tgtPathStr);
            if (targetPath.exists()) targetPath.delete();
        } else {
            logger.log(new Chars("Target directory not defined"), Logger.LEVEL_WARNING);
        }

    }

}
