
package science.unlicense.project.build.jdk;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public final class Tools {

    public static Path findTool(Path javaHome, Chars toolName) throws IOException {
        Path bin = javaHome.resolve(new Chars("bin"));
        Path tool = bin.resolve(toolName);
        if (tool.exists()) return tool;
        //check windows path
        tool = bin.resolve(toolName.concat(new Chars(".exe")));
        if (tool.exists()) return tool;
        throw new IOException("Tool "+toolName+" not found");
    }

}
