
package science.unlicense.project.build.core.dependency;

import java.util.Objects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class Artifact {

    public final ArtifactIdentifier identifier;
    public final Path path;

    public Artifact(ArtifactIdentifier identifier, Path path) {
        this.identifier = identifier;
        this.path = path;
    }

    /**
     * Get artifact sibling, with a different type.
     * @param type
     * @return
     */
    public Artifact sibling(Chars type) {
        final ArtifactIdentifier aid = new ArtifactIdentifier(identifier.groupId, identifier.moduleId, identifier.version, type);
        final Path apath = path.getParent().resolve(aid.getArtifactName());
        return new Artifact(aid, apath);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.identifier);
        hash = 59 * hash + Objects.hashCode(this.path);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Artifact other = (Artifact) obj;
        if (!Objects.equals(this.identifier, other.identifier)) {
            return false;
        }
        if (!Objects.equals(this.path, other.path)) {
            return false;
        }
        return true;
    }



}
