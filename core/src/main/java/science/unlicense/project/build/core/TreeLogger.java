
package science.unlicense.project.build.core;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.AbstractLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.LoggerException;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 *
 * @author Johann Sorel
 */
public class TreeLogger extends AbstractLogger {

    private Chars spacing = Chars.EMPTY;
    private float logLevel = Logger.LEVEL_DEBUG;

    public float getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(float logLevel) {
        this.logLevel = logLevel;
    }

    public void incrementDepth() {
        spacing = spacing.concat(new Chars("  "));
    }

    public void decrementDepth() {
        spacing = spacing.truncate(0, spacing.getCharLength()-2);
    }

    @Override
    public DocumentType getParametersType() {
        return null;
    }

    @Override
    public Document getParameters() {
        return new DefaultDocument();
    }

    @Override
    public void setParameters(Document config) throws LoggerException {
    }

    @Override
    public void log(Chars message, Throwable exception, float level) {
        if (level < logLevel) return;

        if (logLevel <= Logger.LEVEL_DEBUG) {
            if (level <= LEVEL_DEBUG) {
                System.out.print("DEBUG | ");
            } else if (level <= LEVEL_INFORMATION) {
                System.out.print("INFO  | ");
            } else if (level <= LEVEL_WARNING) {
                System.out.print("WARN  | ");
            } else {
                System.out.print("CRIT  | ");
            }
        }

        final Chars[] parts = message.split('\n');
        for (Chars p : parts) {
            System.out.print(spacing);
            System.out.print(p);
            System.out.print('\n');
        }
    }

}
