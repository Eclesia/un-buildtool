
package science.unlicense.project.build.doc;

import science.unlicense.code.api.meta.DefaultMeta;
import science.unlicense.common.api.character.Chars;



/**
 *
 * @author Johann Sorel
 */
public class MavenMeta extends DefaultMeta{

    public static final Chars ID = new Chars("maven-identifier");

    public final Chars groupId;
    public final Chars artifactId;
    public final Chars version;

    public MavenMeta(Chars groupId, Chars artifactId, Chars version) {
        super(ID);
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
    }



}
