
package science.unlicense.project.build.core.dependency;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public interface DependencyResolver {

    ArtifactIdentifier[] listTransitive(ArtifactIdentifier dependency) throws IOException;

    Path find(ArtifactIdentifier dependency) throws IOException;

    Path store(ArtifactIdentifier dependency, Path artifact, ArtifactIdentifier[] transitive) throws IOException;

}
