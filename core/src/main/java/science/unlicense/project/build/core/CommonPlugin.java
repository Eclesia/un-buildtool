
package science.unlicense.project.build.core;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;

/**
 *
 * @author Johann Sorel
 */
public abstract class CommonPlugin {

    public static final Chars JAVA_HOME = new Chars("JAVA_HOME");

    public static final Chars ROOT_PATH = new Chars("rootPath");
    public static final Chars TARGETBASE_PATH = new Chars("targetBasePath");
    public static final Chars GROUP_ID = new Chars("groupId");
    public static final Chars MODULE_ID = new Chars("moduleId");
    public static final Chars VERSION = new Chars("version");
    public static final Chars TYPE = new Chars("type");
    public static final Chars REPOSITORIES = new Chars("repositories");
    public static final Chars DEPENDENCIES = new Chars("dependencies");

    protected Document params;
    protected ArtifactIdentifier currentArtifactIdentifier;
    protected TreeLogger logger;

    public void execute(Document params, Logger logger) throws Exception {
        this.params = params;
        this.logger = (TreeLogger) logger;
        final Chars groupId = (Chars) params.getPropertyValue(GROUP_ID);
        final Chars moduleId = (Chars) params.getPropertyValue(MODULE_ID);
        final Chars version = getValue(params,VERSION);
        final Chars type = (Chars) params.getPropertyValue(TYPE);
        currentArtifactIdentifier = new ArtifactIdentifier(groupId, moduleId, version, type);
    }

    public static Chars getValue(Document doc, Chars name) {
        final Object val = doc.getPropertyValue(name);
        if (val instanceof Chars) {
            return (Chars)val;
        } else if(val instanceof Object) {
            return CObjects.toChars(val);
        }
        return null;
    }

    public static Chars[] getValues(Document doc, Chars name) {
        final Object val = doc.getPropertyValue(name);
        if (val instanceof Chars) {
            return new Chars[]{(Chars)val};
        } else if(val instanceof Object[]) {
            final Object[] array = (Object[]) val;
            final Chars[] values = new Chars[array.length];
            for (int i=0;i<array.length;i++) {
                values[i] = CObjects.toChars(array[i]);
            }
            return values;
        } else if (val == null) {
            return new Chars[0];
        } else {
            return new Chars[]{CObjects.toChars(val)};
        }
    }

    protected static ArtifactIdentifier[] getDependencies(Document doc) {
        return getDependencies(doc,DEPENDENCIES);
    }

    protected static ArtifactIdentifier[] getDependencies(Document doc, Chars propertyName) {
        final Object val = doc.getPropertyValue(propertyName);
        if (val instanceof Document) {
            final Document rd = (Document) val;
            final Chars group = (Chars) rd.getPropertyValue(GROUP_ID);
            final Chars module = (Chars) rd.getPropertyValue(MODULE_ID);
            final Chars version = getValue(rd,VERSION);
            final Chars type = (Chars) rd.getPropertyValue(TYPE);
            return new ArtifactIdentifier[]{new ArtifactIdentifier(group, module, version, type)};
        } else if(val instanceof Object[]) {
            final Object[] array = (Object[]) val;
            final ArtifactIdentifier[] values = new ArtifactIdentifier[array.length];
            for (int i=0;i<array.length;i++) {
                final Document rd = (Document) array[i];
                final Chars group = (Chars) rd.getPropertyValue(GROUP_ID);
                final Chars module = (Chars) rd.getPropertyValue(MODULE_ID);
                final Chars version = getValue(rd,VERSION);
                final Chars type = (Chars) rd.getPropertyValue(TYPE);
                values[i] = new ArtifactIdentifier(group, module, version, type);
            }
            return values;
        } else if (val == null) {
            return new ArtifactIdentifier[0];
        } else {
            throw new RuntimeException("Unexpected dependency value : "+val);
        }
    }

    protected static Sequence listJavaPackages(Collection sourcePaths) throws IOException {
        final Sequence buffer = new ArraySequence();
        final Iterator ite = sourcePaths.createIterator();
        while (ite.hasNext()) {
            listJavaPackages((Path)ite.next(), buffer);
        }
        return buffer;
    }

    private static Sequence listJavaPackages(Path sourcePath, Sequence buffer) throws IOException {
        if (buffer==null) buffer = new ArraySequence();
        listJavaPackages(sourcePath, sourcePath, buffer);
        return buffer;
    }

    private static void listJavaPackages(Path rootPath, Path sourcePath, Sequence packages) throws IOException {

        if (sourcePath.isContainer()) {
            boolean onlySubDirectory = true;
            Iterator ite = sourcePath.getChildren().createIterator();
            while (ite.hasNext()) {
                Path p = (Path) ite.next();
                if (p.isContainer()) {
                    listJavaPackages(rootPath, p, packages);
                } else {
                    onlySubDirectory = false;
                }
            }

            if (sourcePath!=rootPath && !onlySubDirectory) {
                final Chars relative = Paths.relativePath(rootPath, sourcePath);
                String r = relative.toJVMString().replace('/', '.');
                while (r.charAt(0)=='.') r = r.substring(1);
                packages.add(r);
            }
        }

    }

    protected static Set listJavaFiles(Collection sourcePaths, Set buffer) throws IOException {
        final Iterator ite = sourcePaths.createIterator();
        while (ite.hasNext()) {
            buffer = listJavaFiles((Path)ite.next(), buffer);
        }
        return buffer;
    }

    protected static Set listJavaFiles(Path sourcePath, Set buffer) throws IOException {
        if (buffer==null) buffer = new HashSet();

        if (sourcePath.isContainer()) {
            Iterator ite = sourcePath.getChildren().createIterator();
            while (ite.hasNext()) {
                Path p = (Path) ite.next();
                listJavaFiles(p, buffer);
            }
        } else if(sourcePath.getName().endsWith(new Chars(".java"))) {
            buffer.add(sourcePath);
        }

        return buffer;
    }

}
