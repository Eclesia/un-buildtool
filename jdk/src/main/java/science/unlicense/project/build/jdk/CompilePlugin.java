package science.unlicense.project.build.jdk;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 *
 * @author Johann Sorel
 */
public class CompilePlugin extends CommonPlugin {

    public static final Chars SOURCE_PATH = new Chars("sourcePath");
    public static final Chars RESOURCE_PATH = new Chars("resourcePath");
    public static final Chars TARGET_PATH = new Chars("targetPath");
    public static final Chars USE_CONSOLE = new Chars("useConsole");
    public static final Chars GENERATE_MODULE_INFO = new Chars("generateModuleInfo");
    public static final Chars COMPILE_DEPENDENCIES = new Chars("compileDependencies");


    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);


        final Object pathStrs = params.getPropertyValue(SOURCE_PATH);
        final Object pathRscs = params.getPropertyValue(RESOURCE_PATH);
        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(TARGET_PATH);
        final boolean useConsole = Boolean.TRUE.equals(params.getPropertyValue(USE_CONSOLE));
        final boolean generateModuleInfo = Boolean.TRUE.equals(params.getPropertyValue(GENERATE_MODULE_INFO));

        final Path javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Sequence sourcePaths = new ArraySequence();
        if (pathStrs instanceof Object[]) {
            for(Object c : (Object[]) pathStrs) {
                sourcePaths.add(rootPath.resolve((Chars)c));
            }
        } else {
            sourcePaths.add(rootPath.resolve((Chars)pathStrs));
        }
        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        final Path targetPath = targetBasePath.resolve(tgtPathStr);
        targetPath.createContainer();
        final Chars[] repositories = getValues(params, REPOSITORIES);
        final ArtifactIdentifier[] directDependencies = getDependencies(params, DEPENDENCIES);
        final ArtifactIdentifier[] compileDependencies = getDependencies(params, COMPILE_DEPENDENCIES);
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);

        //resolve all dependencies
        final Sequence allDependencies = new ArraySequence(DependencyResolvers.importTransitive(directDependencies, resolvers));
        final Sequence allCompileDependencies = new ArraySequence(DependencyResolvers.importTransitive(compileDependencies, resolvers));

        final Sequence depPaths = new ArraySequence();
        if (directDependencies.length > 0) {
            Iterator ite = allDependencies.createIterator();
            while (ite.hasNext()) {
                Artifact art = (Artifact) ite.next();
                depPaths.add(art.path);
            }
        }
        if (compileDependencies.length > 0) {
            Iterator ite = allCompileDependencies.createIterator();
            while (ite.hasNext()) {
                Artifact art = (Artifact) ite.next();
                if (!allDependencies.contains(art)) {
                    depPaths.add(art.path);
                }
            }
        }

        gen :
        if (generateModuleInfo) {
            //check if a moduleinfo already exist
            final Set javaFiles = listJavaFiles(sourcePaths, null);
            final Iterator jite = javaFiles.createIterator();
            while (jite.hasNext()) {
                final Path p = (Path) jite.next();
                if (p.getName().equals(new Chars("module-info.java"))) {
                    break gen;
                }
            }

            logger.info(new Chars("module-info.java not found, creating one."));
            //add java9 module file
            final StringBuilder sb = new StringBuilder();
            sb.append("module ").append(currentArtifactIdentifier.groupId+"."+currentArtifactIdentifier.moduleId).append(" {\n");
            //list dependencies
            Iterator depite = allDependencies.createIterator();
            while (depite.hasNext()) {
                Artifact aid = (Artifact) depite.next();
                sb.append("requires ").append(aid.identifier.groupId).append('.').append(aid.identifier.moduleId).append(";\n");
            }
            //add compile dependencies as static
            //todo, perhaps not the best approach, maybe using providers/uses ?
            Iterator compdepite = allCompileDependencies.createIterator();
            while (compdepite.hasNext()) {
                Artifact aid = (Artifact) compdepite.next();
                if (!allDependencies.contains(aid)) {
                    sb.append("requires static ").append(aid.identifier.groupId).append('.').append(aid.identifier.moduleId).append(";\n");
                }
            }

            //export all packages
            final Sequence packages = listJavaPackages(sourcePaths);
            final Iterator ite = packages.createIterator();
            while (ite.hasNext()) {
                Object p = ite.next();
                sb.append("exports ").append((String)p).append(";\n");
            }
            sb.append("}");
            Path modFile = targetBasePath.resolve(new Chars("jvm9"));
            modFile.createContainer();
            modFile = modFile.resolve(new Chars("module-info.java"));
            ByteOutputStream in = modFile.createOutputStream();
            CharOutputStream ds = new CharOutputStream(in);
            ds.write(new Chars(sb.toString()));
            ds.close();
            sourcePaths.add(modFile.getParent());
        }


        //compile
        final Compiler compiler = new Compiler();
        compiler.setGenerateModule(generateModuleInfo);
        compiler.setJavaHome(javaHome);
        compiler.setSourcePath((Path[]) sourcePaths.toArray(Path.class));
        compiler.setTargetPath(targetPath);
        compiler.setClasspathPaths((Path[]) depPaths.toArray(Path.class));
        compiler.setUseConsoleTool(useConsole);
        compiler.setLogger(logger);
        compiler.execute();

        //copy resources
        if (pathRscs instanceof Object[]) {
            for (Object c : (Object[]) pathRscs) {
                Path p = rootPath.resolve((Chars) c);
                if (p.isContainer()) {
                    Iterator ite = p.getChildren().createIterator();
                    while (ite.hasNext()) {
                        Path cp = (Path) ite.next();
                        Paths.copy(cp, targetPath.resolve(cp.getName()));
                    }
                } else {
                    Paths.copy(p, targetPath.resolve(p.getName()));
                }
            }
        } else {
            Path p = rootPath.resolve((Chars)pathRscs);
            if (p.isContainer()) {
                Iterator ite = p.getChildren().createIterator();
                while (ite.hasNext()) {
                    Path cp = (Path) ite.next();
                    Paths.copy(cp, targetPath.resolve(cp.getName()));
                }
            } else {
                Paths.copy(p, targetPath.resolve(p.getName()));
            }
        }
    }

}
