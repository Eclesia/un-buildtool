package science.unlicense.project.build.jsweet;

import java.io.File;
import org.jsweet.JSweetConfig;
import org.jsweet.transpiler.EcmaScriptComplianceLevel;
import org.jsweet.transpiler.JSweetFactory;
import org.jsweet.transpiler.JSweetProblem;
import org.jsweet.transpiler.JSweetTranspiler;
import org.jsweet.transpiler.SourceFile;
import org.jsweet.transpiler.SourcePosition;
import org.jsweet.transpiler.TranspilationHandler;
import org.jsweet.transpiler.util.ErrorCountTranspilationHandler;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;
import static science.unlicense.project.build.core.CommonPlugin.REPOSITORIES;
import static science.unlicense.project.build.core.CommonPlugin.ROOT_PATH;
import static science.unlicense.project.build.core.CommonPlugin.TARGETBASE_PATH;
import static science.unlicense.project.build.core.CommonPlugin.getValues;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 *
 * @author Johann Sorel
 */
public class JSweetPlugin extends CommonPlugin {

    public static final Chars SOURCE_PATH = new Chars("sourcePath");
    public static final Chars TARGET_PATH = new Chars("targetPath");

    private String webjarsDir;
    private String typingsDir;

    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params, logger);

        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(TARGET_PATH);
        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        final Path targetPath = targetBasePath.resolve(tgtPathStr);
        final Path javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        final Path metaInf = targetPath.resolve(new Chars("META-INF"));
        metaInf.createContainer();

        webjarsDir = Paths.systemPath(metaInf.resolve(new Chars("resources/webjars/"+currentArtifactIdentifier.moduleId+"/"+currentArtifactIdentifier.version))).toString();
        typingsDir = Paths.systemPath(metaInf.resolve(new Chars("resources/typings/"+currentArtifactIdentifier.moduleId+"/"+currentArtifactIdentifier.version))).toString();
        final Chars jdkHome = Paths.systemPath(javaHome);
        JSweetConfig.initClassPath(jdkHome.toString());

        final JSweetTranspiler transpiler = createJSweetTranspiler();
        transpile(transpiler);

        //generate candy metadata
        //don't know why the transpiler do not generate this ?
        final Path candyMetaJson = metaInf.resolve(new Chars("candy-metadata.json"));
        final CharOutputStream cs = new CharOutputStream(candyMetaJson.createOutputStream());
        cs.write(new Chars("{\"transpilerVersion\": \""+JSweetConfig.getVersionNumber()+"\"}"));
        cs.close();

    }

    private SourceFile[] getSourceFiles() throws Exception {

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Object pathStrs = params.getPropertyValue(SOURCE_PATH);
        final Sequence sourcePaths = new ArraySequence();
        if (pathStrs instanceof Object[]) {
            for(Object c : (Object[])pathStrs) {
                sourcePaths.add(rootPath.resolve((Chars)c));
            }
        } else {
            sourcePaths.add(rootPath.resolve((Chars)pathStrs));
        }
        final Set javaFiles = listJavaFiles(sourcePaths, null);
        final SourceFile[] sources = new SourceFile[javaFiles.getSize()];
        final Iterator ite = javaFiles.createIterator();
        for (int i=0;ite.hasNext();i++) {
            final Path p = (Path) ite.next();
            sources[i] = new SourceFile(new File(Paths.systemPath(p).toString()));
        }
        return sources;
    }

    protected JSweetTranspiler createJSweetTranspiler() throws Exception {

        //build classpath
        final Chars[] repositories = getValues(params, REPOSITORIES);
        final ArtifactIdentifier[] directDependencies = getDependencies(params);
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);
        final Artifact[] dependenciesFiles = DependencyResolvers.importTransitive(directDependencies, resolvers);
        final CharBuffer cb = new CharBuffer();
        for (int i=0;i<dependenciesFiles.length;i++) {
            if (i!=0) cb.append(System.getProperty("path.separator"));
            cb.append(Paths.systemPath(dependenciesFiles[i].path));
        }
        final Chars classPath = cb.toChars();

        //get work folders
        final Chars tgtBasePathStr   = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Path rootPath          = (Path) params.getPropertyValue(ROOT_PATH);
        final Path targetBasePath    = rootPath.resolve(tgtBasePathStr);
        final File baseDir           = new File(Paths.systemPath(targetBasePath).toString());
        final File jsOutDir          = new File(this.webjarsDir);
        final File declarationOutDir = new File(this.typingsDir);

        //create transpiler
        final JSweetFactory factory = new JSweetFactory();
        final JSweetTranspiler transpiler = new JSweetTranspiler(baseDir, null, factory, null, null, jsOutDir, null, classPath.toString());
        transpiler.setTscWatchMode(false);
        transpiler.setEcmaTargetVersion(EcmaScriptComplianceLevel.ES3);
        transpiler.setGenerateDeclarations(true);
        transpiler.setBundle(true);
        transpiler.setDeclarationsOutputDir(declarationOutDir);
        transpiler.setJsOutputDir(jsOutDir);

        return transpiler;
    }

    protected void transpile(JSweetTranspiler transpiler) throws Exception {
        final ErrorCountTranspilationHandler transpilationHandler = new ErrorCountTranspilationHandler(new LogHandler());
        final SourceFile[] sources = getSourceFiles();
        transpiler.transpile(transpilationHandler, sources);

        final int nbError = transpilationHandler.getErrorCount();
        final int nbWarning = transpilationHandler.getWarningCount();
        logger.info(new Chars("errors : "+nbError+"  warnings : "+nbWarning));

        if (nbError > 0) throw new Exception("Transpilation failed");
    }

    private class LogHandler implements TranspilationHandler {

        @Override
        public void report(JSweetProblem problem, SourcePosition sourcePosition, String message) {
            if (sourcePosition == null || sourcePosition.getFile() == null) {
                logger.info(new Chars(problem.getSeverity().name()+" "+message));
            } else {
                logger.info(new Chars(problem.getSeverity().name()+" "+sourcePosition.getFile()+" : "+sourcePosition.getStartLine()+" : "+message));
            }
        }

        @Override
        public void onCompleted(JSweetTranspiler transpiler, boolean fullPass, SourceFile[] files) {
        }

    }

}
