
package science.unlicense.project.build.jdk;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;
import static science.unlicense.project.build.core.CommonPlugin.ROOT_PATH;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 *
 * @author Johann Sorel
 */
public class TestPlugin extends CommonPlugin {

    public static final Chars TEST_DEPENDENCIES = new Chars("testDependencies");
    
    private static final Chars TESTED_CLASS = new Chars("testedClass");
    private static final Chars TESTED_METHOD = new Chars("testedMethod");
    private static final Chars TEST_ROOT = new Chars("testPath");

    private static final Chars EXT_JAVA = new Chars(".java");
    private static final Chars EXT_CLASS = new Chars(".class");

    private static final String BEFORE_CLASS = "beforeClass";
    private static final String AFTER_CLASS = "afterClass";
    private static final String BEFORE_TEST = "beforeTest";
    private static final String AFTER_TEST = "afterTest";
    
    private Path rootPath;
    private Path testPath;
    private Path testCompiledPath;
    private Path javaHome;

    private Chars testedClass;
    private Chars testedMethod;
    private Chars[] repositories;
    private ArtifactIdentifier[] dependencies;
    private ArtifactIdentifier[] testDependencies;

    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        testPath = rootPath.resolve((Chars) params.getPropertyValue(TEST_ROOT));
        javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        testCompiledPath = rootPath.resolve(new Chars("target2")).resolve(new Chars("testClasses"));
        repositories = getValues(params, REPOSITORIES);
        dependencies = getDependencies(params, DEPENDENCIES);
        testDependencies = getDependencies(params, TEST_DEPENDENCIES);
        testedClass = (Chars) params.getPropertyValue(TESTED_CLASS);
        testedMethod = (Chars) params.getPropertyValue(TESTED_METHOD);

        //compile test classes
        final Sequence compiledTestClasses = compileTestClasses();


        //create the test classloader
        final Sequence depPaths = listDependencies();
        final List<URL> classpath = new ArrayList<>();
        try {
            Iterator ite = depPaths.createIterator();
            while (ite.hasNext()) {
                final Path next = (Path) ite.next();
                classpath.add(new URL(next.toURI().toString()));
            }
            classpath.add(new URL(testCompiledPath.toURI().toString()+"/"));

        } catch (Exception ex) {
            throw new Exception(ex.getMessage(), ex);
        }
        final URLClassLoader loader = new URLClassLoader(classpath.toArray(new URL[0]));

        //list the test classes to execute
        final List<Chars> testClasses = new ArrayList<>();
        final List<Path> lst = listSourceFiles(testPath, null);
        for (Path f : lst) {
            Chars className = Paths.relativePath(testPath, f).replaceAll(File.separatorChar, '.');
            className = className.truncate(0, className.getLastOccurence('.'));
            while (className.getUnicode(0)=='.' ||className.getUnicode(0)=='/') {
                className = className.truncate(1,-1);
            }
            if (testedClass!=null && !testedClass.equals(className)) continue;
            testClasses.add(className);
        }

        boolean globalSucess = true;
        for(Chars f : testClasses) {
            if (!f.endsWith(new Chars("Test"))) continue;
            logger.info(new Chars("Running tests : "+f));
            try {
                final Class<?> candidate = loader.loadClass(f.toString());
                if ((candidate.getModifiers() & Modifier.ABSTRACT) != 0) continue;
                globalSucess &= executeTestClass(candidate);
            } catch (ClassNotFoundException | InstantiationException |
                     IllegalAccessException | InvocationTargetException ex) {
                throw new Exception(ex.getMessage(), ex);
            }
        }

        if (!globalSucess) {
            throw new Exception("Tests in error.");
        }
    }

    private Sequence compileTestClasses() throws Exception {

        //resolve all dependencies
        final Sequence depPaths = listDependencies();

        //add this module classes
        final Path compiledJar = rootPath.resolve(new Chars("target2")).resolve(currentArtifactIdentifier.getArtifactName());
        depPaths.add(compiledJar);

        //compile test classes
        final Compiler compiler = new Compiler();
        compiler.setJavaHome(javaHome);
        compiler.setSourcePath(new Path[]{testPath});
        compiler.setTargetPath(testCompiledPath);
        compiler.setClasspathPaths((Path[]) depPaths.toArray(Path.class));
        return compiler.execute();
    }

    private boolean executeTestClass(Class clazz) throws InstantiationException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        boolean globalSucess = true;

        final Method beforeClass = getMethod(clazz, BEFORE_CLASS);
        final Method afterClass = getMethod(clazz, AFTER_CLASS);
        final Method beforeTest = getMethod(clazz, BEFORE_TEST);
        final Method afterTest = getMethod(clazz, AFTER_TEST);

        final Object instance = clazz.newInstance();

        if (beforeClass!=null) beforeClass.invoke(instance);

        int nb = 0;
        int sucess = 0;
        for (Method m : clazz.getMethods()) {
            if(!m.getName().startsWith("test")) continue;
            if((m.getModifiers() & Modifier.ABSTRACT) != 0) continue;
            if(m.getParameterCount()!=0) continue;
            if (testedMethod!=null && !testedMethod.equals(m.getName())) continue;
            nb++;

            if (beforeTest!=null) beforeTest.invoke(instance);

            try {
                m.invoke(instance);
                sucess++;
            }catch(InvocationTargetException ex) {
                globalSucess = false;
                logger.warning(new Chars("Test fail : "+clazz.getName()+"."+m.getName()+"   "+ex.getMessage()));
                ex.printStackTrace();
            }

            if (afterTest!=null) afterTest.invoke(instance);
        }

        if (afterClass!=null) afterClass.invoke(instance);

        if (nb==sucess) {
            logger.info(new Chars("Results : "+sucess+"/"+nb));
        } else {
            logger.critical(new Chars("Results : "+sucess+"/"+nb));
        }

        return globalSucess;
    }

    private Sequence listDependencies() throws IOException {
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);
        final Artifact[] allDependencies = DependencyResolvers.importTransitive(dependencies, resolvers);
        
        final Sequence depPaths = new ArraySequence();
        if (dependencies.length > 0) {
            for (Artifact art : allDependencies) {
                depPaths.add(art.path);
            }
        }
        if (testDependencies.length > 0) {
            final Artifact[] allTestDependencies = DependencyResolvers.importTransitive(testDependencies, resolvers);
            for (Artifact art : allTestDependencies) {
                if (!Arrays.contains(allDependencies, art)) {
                    depPaths.add(art.path);
                }
            }
        }
        return depPaths;
    }

    private static List<Path> listSourceFiles(Path f, List<Path> sources) throws IOException{
        if(sources == null){
            sources = new ArrayList<>();
        }

        if(f.isContainer()){
            final Collection children = f.getChildren();
            final Iterator ite = children.createIterator();
            while (ite.hasNext()) {
                final Object next = ite.next();
                listSourceFiles((Path) next, sources);
            }
        }else if(f.getName().endsWith(EXT_JAVA) || f.getName().endsWith(EXT_CLASS)){
            sources.add(f);
        }

        return sources;
    }

    private static Method getMethod(Class clazz, String name) {
        try {
            return clazz.getMethod(name);
        } catch (NoSuchMethodException ex) {
            return null;
        } catch (SecurityException ex) {
            return null;
        }
    }

}
