
package science.unlicense.project.build.test;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;
import static science.unlicense.project.build.core.CommonPlugin.JAVA_HOME;
import static science.unlicense.project.build.core.CommonPlugin.REPOSITORIES;
import static science.unlicense.project.build.core.CommonPlugin.ROOT_PATH;
import static science.unlicense.project.build.core.CommonPlugin.getValues;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;
import science.unlicense.project.build.jdk.Compiler;

/**
 *
 * @author Johann Sorel
 */
public class TestPlugin extends CommonPlugin {

    public static final Chars SOURCE_PATH = new Chars("sourcePath");
    public static final Chars TEST_PATH = new Chars("testPath");
    public static final Chars TEST_TARGET = new Chars("testTarget");
    public static final Chars TEST_DEPENDENCIES = new Chars("testDependencies");

    private static final String BEFORE_CLASS = "beforeClass";
    private static final String AFTER_CLASS = "afterClass";
    private static final String BEFORE_TEST = "beforeTest";
    private static final String AFTER_TEST = "afterTest";

    private static final Chars EXT_JAVA = new Chars(".java");
    private static final Chars EXT_CLASS = new Chars(".class");

    private Chars testedClass = null;
    private Chars testedMethod = null;
    private Chars test;

    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params, logger);

        //check if it's a single test file run
        Chars testedParam = (Chars) params.getPropertyValue(TEST_TARGET);
        testedClass = null;
        testedMethod = null;
        if (testedParam != null) {
            final int idx = testedParam.getFirstOccurence('$');
            if (idx>0) {
                testedClass = testedParam.truncate(0, idx);
                testedMethod = testedParam.truncate(idx+1,-1);
            } else {
                testedClass = testedParam;
            }
        }

        final Object srcPathStrs = params.getPropertyValue(SOURCE_PATH);
        final Object tstPathStrs = params.getPropertyValue(TEST_PATH);
        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(new Chars("testClasses"));
        final Path javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        final Path targetPath = targetBasePath.resolve(tgtPathStr);

        final Sequence sourcePaths = new ArraySequence();
        if (srcPathStrs instanceof Object[]) {
            for(Object c : (Object[])srcPathStrs) {
                sourcePaths.add(rootPath.resolve((Chars)c));
            }
        } else {
            sourcePaths.add(rootPath.resolve((Chars)srcPathStrs));
        }

        final Sequence testPaths = new ArraySequence();
        if (tstPathStrs instanceof Object[]) {
            for(Object c : (Object[])tstPathStrs) {
                testPaths.add(rootPath.resolve((Chars)c));
            }
        } else {
            testPaths.add(rootPath.resolve((Chars)tstPathStrs));
        }

        //resolve dependencies
        final Chars[] repositories = getValues(params, REPOSITORIES);
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);

        final Set depPaths = new HashSet();

        {//normal dependencies
            final ArtifactIdentifier[] directDependencies = getDependencies(params,DEPENDENCIES);
            final Artifact[] allDependencies = DependencyResolvers.importTransitive(directDependencies, resolvers);
            if (directDependencies.length>0) {
                for (Artifact art : allDependencies) {
                    depPaths.add(art.path);
                }
            }
        }
        {//test dependencies
            final ArtifactIdentifier[] directDependencies = getDependencies(params,TEST_DEPENDENCIES);
            final Artifact[] allDependencies = DependencyResolvers.importTransitive(directDependencies, resolvers);
            if (directDependencies.length>0) {
                for (Artifact art : allDependencies) {
                    depPaths.add(art.path);
                }
            }
        }
        {//this module : TODO remove this by classes directly
            final ArtifactIdentifier[] directDependencies = new ArtifactIdentifier[]{currentArtifactIdentifier};
            final Artifact[] allDependencies = DependencyResolvers.importTransitive(directDependencies, resolvers);
            if (directDependencies.length>0) {
                for (Artifact art : allDependencies) {
                    depPaths.add(art.path);
                }
            }
        }

        //compile test classes
        final Compiler compiler = new Compiler();
        compiler.setGenerateModule(false);
        compiler.setJavaHome(javaHome);
        compiler.setSourcePath((Path[]) testPaths.toArray(Path.class));
        compiler.setTargetPath(targetPath);
        compiler.setClasspathPaths((Path[]) depPaths.toArray(Path.class));
        compiler.setUseConsoleTool(true);
        compiler.setLogger(logger);
        compiler.execute();


        //create the test classloader
        final List<URL> classpath = new ArrayList<>();
        final Iterator ite = depPaths.createIterator();
        while (ite.hasNext()) {
            final Path p = (Path) ite.next();
            classpath.add(new URL(p.toURI().toString()));
        }
        classpath.add(new URL(targetPath.toURI().toString()+"/"));

        final URLClassLoader loader = new URLClassLoader(classpath.toArray(new URL[0]));


        //list the test classes to execute
        final Sequence testFiles = new ArraySequence();
        listSourceFiles(targetPath, testFiles);
        final List<String> testClasses = new ArrayList<>();
        for (int i=0,n=testFiles.getSize();i<n;i++) {
            final Path f = (Path) testFiles.get(i);
            final Chars relativePath = Paths.relativePath(targetPath, f);
            Chars className = relativePath.replaceAll(File.separatorChar, '.');
            className = className.truncate(0, className.getLastOccurence('.'));
            while (className.getUnicode(0)=='.') className = className.truncate(1,-1);
            if (testedClass!=null && !testedClass.equals(className)) continue;
            testClasses.add(className.toString());
        }


        boolean globalSucess = true;
        for (String f : testClasses) {
            if (!f.endsWith("Test")) continue;
            logger.info(new Chars("Running tests : "+f));
            try {
                final Class<?> candidate = loader.loadClass(f);
                if ((candidate.getModifiers() & Modifier.ABSTRACT) != 0) continue;
                globalSucess &= executeTestClass(candidate);
            } catch (ClassNotFoundException | InstantiationException |
                     IllegalAccessException | InvocationTargetException ex) {
                throw new Exception(ex.getMessage(), ex);
            }
        }

        if (!globalSucess) {
            throw new Exception("Tests in error.");
        }
    }

    private boolean executeTestClass(Class clazz) throws InstantiationException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        boolean globalSucess = true;

        final Method beforeClass = getMethod(clazz, BEFORE_CLASS);
        final Method afterClass = getMethod(clazz, AFTER_CLASS);
        final Method beforeTest = getMethod(clazz, BEFORE_TEST);
        final Method afterTest = getMethod(clazz, AFTER_TEST);

        final Object instance = clazz.newInstance();

        if (beforeClass!=null) beforeClass.invoke(instance);

        int nb = 0;
        int sucess = 0;
        for (Method m : clazz.getMethods()) {
            if (!m.getName().startsWith("test")) continue;
            if ((m.getModifiers() & Modifier.ABSTRACT) != 0) continue;
            if (m.getParameterCount()!=0) continue;
            if (testedMethod!=null && !testedMethod.equals(m.getName())) continue;
            nb++;

            if (beforeTest != null) beforeTest.invoke(instance);

            try {
                m.invoke(instance);
                sucess++;
            } catch (InvocationTargetException ex) {
                globalSucess = false;
                logger.critical(new Chars("Test fail : "+clazz.getName()+"."+m.getName()+"   "+ex.getMessage()),ex);
            }

            if (afterTest != null) afterTest.invoke(instance);
        }

        if (afterClass != null) afterClass.invoke(instance);

        if (nb == sucess) {
            logger.info(new Chars("Results : "+sucess+"/"+nb));
        } else {
            logger.critical(new Chars("Results : "+sucess+"/"+nb));
        }

        return globalSucess;
    }

    private static Sequence listSourceFiles(Path base, Sequence sources) throws IOException {
        if (sources == null){
            sources = new ArraySequence();
        }

        if (base.isContainer()){
            final Iterator ite = base.getChildren().createIterator();
            while (ite.hasNext()) {
                listSourceFiles((Path)ite.next(), sources);
            }
        } else if (base.getName().endsWith(EXT_JAVA) || base.getName().endsWith(EXT_CLASS)){
            sources.add(base);
        }

        return sources;
    }

    private static Method getMethod(Class clazz, String name) {
        try {
            return clazz.getMethod(name);
        } catch (NoSuchMethodException ex) {
            return null;
        } catch (SecurityException ex) {
            return null;
        }
    }

}
