

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 *
 * @author Johann Sorel
 */
public class JDKDaemon {

    public static void main(String[] args) throws Exception {
        new JDKDaemon(Integer.valueOf(args[0]));

    }

    private final Socket clientSocket;
    private final DataInputStream in;
    private final DataOutputStream out;

    private JDKDaemon(int port) throws Exception{
        clientSocket = new Socket("localhost", port);
        //System.out.println("Deamon connected on port "+port);
        in = new DataInputStream(clientSocket.getInputStream());
        out = new DataOutputStream(clientSocket.getOutputStream());

        for (int b = in.read(); b != 0; b=in.read()) {

            if (b==1) {
                compile();
            } else if (b==255) {
                //shutdown
                System.out.println("Compile deamon shuting down");
                clientSocket.close();
                return;
            } else {
                System.out.println("UNVALID CODE "+b);
                clientSocket.close();
                System.exit(13);
            }

        }
        System.out.println("END");

    }


    private void compile() throws IOException {
        final String classpath = readString();
        final String target = readString();
        final boolean generateModule = readBoolean();
        final String[] javaPaths = readStrings();
        final List<File> args = new ArrayList<>(javaPaths.length);
        for (int i=0;i<javaPaths.length;i++) {
            args.add(new File(javaPaths[i]));
        }

        if (in.read() != 255) {
            throw new IOException("Unvalid compile end marker");
        }


        final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();


        final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        final StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
        try {
            final Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(args);
            final ArrayList<String> options = new ArrayList<>();
            if (classpath != null) {
                options.add("-classpath");
                options.add(classpath);

                if (generateModule) {
                    options.add("--module-path="+classpath);
                }
            }
//            options.add("-source");
//            options.add("1.8");
//            options.add("-target");
//            options.add("1.8");

            options.add("-d");
            options.add(target);
            final JavaCompiler.CompilationTask task = compiler.getTask(null,fileManager,diagnostics,options,null,compilationUnits);
            if (!task.call()) {
                final Locale myLocale = Locale.getDefault();
                final StringBuilder msg = new StringBuilder();
                msg.append("Cannot compile to Java bytecode:");
                for (Diagnostic<? extends JavaFileObject> err : diagnostics.getDiagnostics()) {
                    msg.append('\n');
                    msg.append(err.getKind());
                    msg.append(": ");
                    if (err.getSource() != null) {
                        msg.append(err.getSource().getName());
                    }
                    msg.append(':');
                    msg.append(err.getLineNumber());
                    msg.append(": ");
                    msg.append(err.getMessage(myLocale));
                }
                throw new Exception(msg.toString());
            }
            //send compile done marker
            out.write(200);
            out.flush();
        } catch (Throwable ex) {
            ex.printStackTrace();
            //send compile done marker
            out.write(500);
            out.flush();
        } finally {
            fileManager.close();
        }


    }

    private String[] readStrings() throws IOException {
        final int nb = in.readInt();
        final String[] strings = new String[nb];
        for (int i=0; i<nb; i++) {
            strings[i] = readString();
        }
        return strings;
    }

    private String readString() throws IOException {
        final int classpathSize = in.readInt();
        final byte[] classpathBuffer = new byte[classpathSize];
        in.readFully(classpathBuffer);
        return new String(classpathBuffer, "UTF-8");
    }

    private boolean readBoolean() throws IOException {
        return in.read() != 0;
    }
}
