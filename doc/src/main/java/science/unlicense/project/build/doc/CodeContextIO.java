
package science.unlicense.project.build.doc;

import science.unlicense.code.api.CodeContext;
import science.unlicense.code.api.Function;
import science.unlicense.code.api.Property;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.json.JSONUtilities;
import science.unlicense.format.json.JSONWriter;

/**
 *
 * @author Johann Sorel
 */
public class CodeContextIO {

    private static final Chars TYPE_CLASS = new Chars(science.unlicense.code.api.Class.class.getName());

    private static final Chars KEY_TYPE = new Chars("type");
    private static final Chars KEY_PROPERTIES = new Chars("properties");
    private static final Chars KEY_FUNCTIONS = new Chars("functions");

    public static void read(Path path, CodeContext context) throws IOException, ClassNotFoundException {
        final Dictionary doc = JSONUtilities.readAsDictionary(path);
        final Iterator ite = doc.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Chars key = (Chars) pair.getValue1();
            final Dictionary subDoc = (Dictionary) pair.getValue2();
            final Chars type = (Chars) subDoc.getValue(KEY_TYPE);
            final Object[] properties = (Object[]) subDoc.getValue(KEY_PROPERTIES);
            final Object[] functions = (Object[]) subDoc.getValue(KEY_FUNCTIONS);

            if (type.equals(TYPE_CLASS)) {
                for (int i=0;i<functions.length;i++) {
                    Function func = new Function();
                    func.id = (Chars) functions[i];
                    functions[i] = func;
                }
                for (int i=0;i<properties.length;i++) {
                    Property property = new Property();
                    property.id = (Chars) properties[i];
                    properties[i] = property;
                }

                final science.unlicense.code.api.Class clazz = new science.unlicense.code.api.Class();
                clazz.id = key;
                clazz.properties.addAll(properties);
                clazz.functions.addAll(functions);
                context.elements.add(key, clazz);
            }
        }
    }

    public static void write(Path path, CodeContext context) throws IOException {
        final JSONWriter writer = new JSONWriter();
        writer.setOutput(path);
        writer.setFormatted(true);
        writer.setEncoding(CharEncodings.UTF_8);
        writer.setIndentation(new Chars("  "));
        writer.writeObjectBegin();

        final Iterator iterator = context.elements.getPairs().createIterator();
        while (iterator.hasNext()) {
            final Pair pair = (Pair) iterator.next();
            final CharArray key = (CharArray) pair.getValue1();
            final Object value = pair.getValue2();

            if (value instanceof science.unlicense.code.api.Class) {
                writer.writeName(key.toChars());
                writer.writeObjectBegin();
                writer.writeProperty(KEY_TYPE, TYPE_CLASS);

                final science.unlicense.code.api.Class clazz = (science.unlicense.code.api.Class) value;
                final Sequence properties = new ArraySequence();
                final Iterator propIte = clazz.properties.createIterator();
                while (propIte.hasNext()) {
                    Property prop = (Property) propIte.next();
                    properties.add(prop.id);
                }
                writer.writeProperty(KEY_PROPERTIES, properties.toArray());

                final Sequence functions = new ArraySequence();
                final Iterator funcIte = clazz.functions.createIterator();
                while (funcIte.hasNext()) {
                    Function prop = (Function) funcIte.next();
                    functions.add(prop.id);
                }
                writer.writeProperty(KEY_FUNCTIONS, functions.toArray());

                writer.writeObjectEnd();
            }
        }
        writer.writeObjectEnd();
    }

}
