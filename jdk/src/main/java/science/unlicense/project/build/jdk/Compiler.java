
package science.unlicense.project.build.jdk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.DefaultLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommandOutputPrinter;
import science.unlicense.project.build.core.TreeLogger;

/**
 *
 * @author Johann Sorel
 */
public class Compiler {

    private Logger logger = new DefaultLogger();
    private Path[] sourcePaths;
    private Path[] modulePaths;
    private Path targetPath;
    private Path javaHome;
    private Path[] classpathPaths;
    private boolean useConsoleTool;
    private boolean generateModule = false;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Path getJavaHome() {
        return javaHome;
    }

    public void setJavaHome(Path javaHome) {
        this.javaHome = javaHome;
    }

    public Path[] getModulePaths() {
        return modulePaths;
    }

    public void setModulePaths(Path[] modulePaths) {
        this.modulePaths = modulePaths;
    }

    public Path[] getSourcePath() {
        return sourcePaths;
    }

    public void setSourcePath(Path[] sourcePath) {
        this.sourcePaths = sourcePath;
    }

    public Path getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(Path targetPath) {
        this.targetPath = targetPath;
    }

    public Path[] getClasspathPaths() {
        return classpathPaths;
    }

    public void setClasspathPaths(Path[] classpathPaths) {
        this.classpathPaths = classpathPaths;
    }

    public boolean isUseConsoleTool() {
        return useConsoleTool;
    }

    public void setUseConsoleTool(boolean useConsoleTool) {
        this.useConsoleTool = useConsoleTool;
    }

    public void setGenerateModule(boolean java9) {
        this.generateModule = java9;
    }

    public boolean isGenerateModule() {
        return generateModule;
    }

    public Sequence execute() throws Exception {
        final Sequence files = new ArraySequence();
        targetPath.createContainer();
        compileClassesDeamon();
//        if (useConsoleTool) {
//            compileClassesConsole();
//        } else {
//            compileClassesJava();
//        }

        listFiles(targetPath, new Chars(".class"), files);
        return files;
    }

    private void compileClassesJava() throws Exception {
        final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        final Chars targetStr = Paths.systemPath(targetPath);

        final List<String> cps = new ArrayList<>();
        if (classpathPaths!=null) {
            for (int i=0;i<classpathPaths.length;i++) {
                listFilesStr(classpathPaths[i], new Chars(".jar"), cps);
                listFilesStr(classpathPaths[i], new Chars(".class"), cps);
            }
        }

        String classpath = null;
        if (!cps.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            for (int i=0;i<cps.size();i++) {
                if(i!=0) sb.append(":");
                sb.append(cps.get(i));
            }
            classpath = sb.toString();
        }

        final List<File> args = new ArrayList<>();
        for(Path srcPath : sourcePaths) {
            listFiles(srcPath, new Chars(".java"), args);
        }
        if (args.isEmpty()) {
            logger.log(new Chars("Nothing to compile."), Logger.LEVEL_INFORMATION);
            return;
        }

        final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        final StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
        try {
            final Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(args);
            final ArrayList<String> options = new ArrayList<>();
            if (classpath != null) {
                options.add("-classpath");
                options.add(classpath);

                if (generateModule) {
                    options.add("--module-path="+classpath);
                }
            }

            options.add("-d");
            options.add(targetStr.toJVMString());
            final JavaCompiler.CompilationTask task = compiler.getTask(null,fileManager,diagnostics,options,null,compilationUnits);
            if (!task.call()) {
                final Locale myLocale = Locale.getDefault();
                final StringBuilder msg = new StringBuilder();
                msg.append("Cannot compile to Java bytecode:");
                for (Diagnostic<? extends JavaFileObject> err : diagnostics.getDiagnostics()) {
                    msg.append('\n');
                    msg.append(err.getKind());
                    msg.append(": ");
                    if (err.getSource() != null) {
                        msg.append(err.getSource().getName());
                    }
                    msg.append(':');
                    msg.append(err.getLineNumber());
                    msg.append(": ");
                    msg.append(err.getMessage(myLocale));
                }
                throw new Exception(msg.toString());
            }
        } finally {
            fileManager.close();
        }
    }

    private void compileClassesConsole() throws Exception {

        final Path javacPath = Tools.findTool(javaHome, new Chars("javac"));

        final List<String> args = new ArrayList<>();
        args.add(Paths.systemPath(javacPath).toString());
        args.add("-d");
        args.add(Paths.systemPath(targetPath).toString());
        for(Path srcPath : sourcePaths) {
            listFilesStr(srcPath, new Chars(".java"), args);
        }
        if (args.size()==3) {
            logger.log(new Chars("Nothing to compile."), Logger.LEVEL_INFORMATION);
            return;
        }

        final List<String> cps = new ArrayList<>();
        if (classpathPaths!=null) {
            for (int i=0;i<classpathPaths.length;i++) {
                listFilesStr(classpathPaths[i], new Chars(".jar"), cps);
                listFilesStr(classpathPaths[i], new Chars(".class"), cps);
            }
        }

        if (!cps.isEmpty()) {
            args.add("-cp");
            final StringBuilder sb = new StringBuilder();
            for (int i=0;i<cps.size();i++) {
                if(i!=0) sb.append(":");
                sb.append(cps.get(i));
            }
            args.add(sb.toString());

            if(generateModule) {
                args.add("--module-path="+sb.toString());
            }
        }

        if (true) {
            final CharBuffer sb2 = new CharBuffer();
            for(String s : args) {
                sb2.append(" "+s);
            }
            logger.debug(sb2.toChars());
        }

        final Process process = Runtime.getRuntime().exec(args.toArray(new String[0]));
        new CommandOutputPrinter(process.getInputStream(),logger);
        new CommandOutputPrinter(process.getErrorStream(),logger);
        int res = process.waitFor();
        if (res!=0) {
            throw new Exception("Compilation fail");
        }
    }


    private static Deamon DEAMON;

    private void compileClassesDeamon() throws Exception {

        if (DEAMON == null) {
            //compile deamon

            final Path javacPath = Tools.findTool(javaHome, new Chars("javac"));
            final Path deamonPath = Paths.resolve(new Chars("mod:/science/unlicense/project/build/jdk/JDKDaemon.java"));
            final Path targetTempPath = Paths.resolveSystem(new Chars(File.createTempFile("deamon", "").getPath()));
            targetTempPath.delete();
            targetTempPath.createContainer();
            final Path deamonTempPath = targetTempPath.resolve(new Chars("JDKDaemon.java"));
            IOUtilities.copy(deamonPath.createInputStream(), deamonTempPath.createOutputStream());

            final List<String> compileArgs = new ArrayList<>();
            compileArgs.add(Paths.systemPath(javacPath).toString());
            compileArgs.add("-d");
            compileArgs.add(Paths.systemPath(targetTempPath).toString());
            compileArgs.add(Paths.systemPath(deamonTempPath).toString());


            final Process process = Runtime.getRuntime().exec(compileArgs.toArray(new String[0]));
            new CommandOutputPrinter(process.getInputStream(),logger);
            new CommandOutputPrinter(process.getErrorStream(),logger);
            int res = process.waitFor();
            if (res!=0) {
                throw new Exception("Compilation fail");
            }

            final Path javaPath = Tools.findTool(javaHome, new Chars("java"));
            final List<String> execArgs = new ArrayList<>();
            execArgs.add(Paths.systemPath(javaPath).toString());
            execArgs.add("-cp");
            execArgs.add(Paths.systemPath(targetTempPath).toString());
            execArgs.add("JDKDaemon");

            DEAMON = new Deamon(execArgs);
        }


        final Chars target = Paths.systemPath(targetPath);

        final List<String> cps = new ArrayList<>();
        if (classpathPaths!=null) {
            for (int i=0;i<classpathPaths.length;i++) {
                listFilesStr(classpathPaths[i], new Chars(".jar"), cps);
                listFilesStr(classpathPaths[i], new Chars(".class"), cps);
            }
        }

        String classpath = "";
        if (!cps.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            for (int i=0;i<cps.size();i++) {
                if(i!=0) sb.append(":");
                sb.append(cps.get(i));
            }
            classpath = sb.toString();
        }

        final List<String> args = new ArrayList<>();
        for(Path srcPath : sourcePaths) {
            listFilesStr(srcPath, new Chars(".java"), args);
        }
        if (args.isEmpty()) {
            logger.log(new Chars("Nothing to compile."), Logger.LEVEL_INFORMATION);
            return;
        }
        
        if ( !(logger instanceof TreeLogger) || (logger instanceof TreeLogger && ((TreeLogger) logger).getLogLevel() <= Logger.LEVEL_DEBUG)) {
            logger.debug(new Chars("Compiling with :"));
            logger.debug(new Chars(" classpath="+classpath));
            logger.debug(new Chars(" target="+target));
            logger.debug(new Chars(" generateModule="+generateModule));
            logger.debug(new Chars(" javafiles="+Arrays.toString(args.toArray())));
        }

        // compile
        DEAMON.out.write(1);
        // classpath
        DEAMON.out.writeInt(classpath.getBytes().length);
        DEAMON.out.write(classpath.getBytes());
        // target
        DEAMON.out.writeInt(target.toBytes().length);
        DEAMON.out.write(target.toBytes());
        // generateModule
        DEAMON.out.write(generateModule ? 1 : 0);
        // java files
        DEAMON.out.writeInt(args.size());
        for (int i=0,n=args.size();i<n;i++) {
            DEAMON.out.writeInt(args.get(i).getBytes().length);
            DEAMON.out.write(args.get(i).getBytes());
        }
        //end marker
        DEAMON.out.write(255);
        DEAMON.out.flush();
        if (!DEAMON.process.isAlive()) {
            throw new Exception("Compilation failed");
        }


        if (DEAMON.in.read() != 200) {
            throw new Exception("Compilation failed");
        }
        logger.debug(new Chars("Deamon build request finished "));


    }


    private static List<File> listFiles(Path root, Chars extension, List<File> sources) throws IOException {
        if (sources == null) {
            sources = new ArrayList<>();
        }

        if (root.isContainer()) {
            final Iterator ite = root.getChildren().createIterator();
            while (ite.hasNext()) {
                final Path child = (Path) ite.next();
                listFiles(child, extension, sources);
            }
        } else if (root.getName().endsWith(extension)) {
            File file = new File(Paths.systemPath(root).toString());
            if(!file.exists()) throw new IOException("ARGGG "+file);
            sources.add(file);
        }

        return sources;
    }

    private static List<String> listFilesStr(Path root, Chars extension, List<String> sources) throws IOException {
        if (sources == null) {
            sources = new ArrayList<>();
        }

        if (root.isContainer()) {
            final Iterator ite = root.getChildren().createIterator();
            while (ite.hasNext()) {
                final Path child = (Path) ite.next();
                listFilesStr(child, extension, sources);
            }
        } else if (root.getName().endsWith(extension)) {
            sources.add(Paths.systemPath(root).toString());
        }

        return sources;
    }

    private static Sequence listFiles(Path root, Chars extension, Sequence sources) throws IOException {

        if (root.isContainer()) {
            final Iterator ite = root.getChildren().createIterator();
            while (ite.hasNext()) {
                final Path child = (Path) ite.next();
                listFiles(child, extension, sources);
            }
        } else if (root.getName().endsWith(extension)) {
            sources.add(root);
        }

        return sources;
    }

    private class Deamon {

        private final ServerSocket serverSocket;
        private final Socket clientSocket;
        private final DataInputStream in;
        private final DataOutputStream out;

        private final Process process;

        public Deamon(List<String> args) throws java.io.IOException {
            serverSocket = new ServerSocket(0);

            args.add(""+serverSocket.getLocalPort());
            process = Runtime.getRuntime().exec(args.toArray(new String[0]));
            new CommandOutputPrinter(process.getInputStream(),logger);
            new CommandOutputPrinter(process.getErrorStream(),logger);

            clientSocket = serverSocket.accept();
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
        }



    }
}
