
package science.unlicense.project.build.app;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.Documents;
import science.unlicense.common.api.number.Int32;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import static science.unlicense.encoding.impl.io.x364.X364.*;
import science.unlicense.project.build.core.Builder;
import science.unlicense.project.build.core.TreeLogger;
import science.unlicense.system.jvm.JVMOutputStream;

/**
 *  Unlicense Build Tool
 *
 *  execution : ubuild -run build -p:arg1 val1 -p:arg2 val2
 *
 * @author Johann Sorel
 */
public class Build {

    private static final Chars HELP = new Chars("-help");
    private static final Chars FILE = new Chars("-file");
    private static final Chars START = new Chars("-start");
    private static final Chars RUN = new Chars("-run");
    private static final Chars PARAM = new Chars("-p:");
    private static final Chars LOGLEVEL = new Chars("-loglevel");

    public static void main(String[] as) throws Exception {

        final long before = System.currentTimeMillis();

        final Chars[] args = toChars(as);

        //parameters
        Path buildFile = Paths.resolve(new Chars("file:./build.hjson"));
        Chars restart = null;
        Chars[] run = null;
        int loglevel = Logger.LEVEL_INFORMATION;

        final Document userParams = new DefaultDocument();

        //parse arguments
        if (args.length==0 || args[0].equals(HELP)) {
            printHelp();
            System.exit(0);
        } else {
            //map parameters
            for(int i=0;i<args.length;i+=2){
                final Chars name = args[i];
                if (FILE.equals(name)) {
                    buildFile = Paths.resolve(new Chars("file:").concat(args[i+1]));
                } else if (START.equals(name)) {
                    restart = args[i+1];
                } else if (RUN.equals(name)) {
                    run = args[i+1].split(',');
                } else if (LOGLEVEL.equals(name)) {
                    loglevel = Int32.decode(args[i+1]);
                } else if (name.startsWith(PARAM)) {
                    final Chars paramName = name.truncate(3, -1);
                    final Chars paramValue = args[i+1];
                    userParams.setPropertyValue(paramName, paramValue);
                } else {
                    final CharOutputStream out = new CharOutputStream(new JVMOutputStream(System.out),CharEncodings.UTF_8);
                    out.write(new Chars("Unvalid parameter '"+name+"' \nFor help use $> ubuild -help")).endLine();
                    System.exit(1);
                }
            }
        }

        final TreeLogger logger = new TreeLogger();
        logger.setLogLevel(loglevel);

        final Builder b = new Builder(buildFile);
        b.setLogger(logger);
        b.setStartFrom(restart);
        b.setRun(run);
        Documents.overrideFields(b.getGlobalParameters(), userParams);
        try {
            b.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("\n《 BUILD FAILED 》");
            System.out.println("Restart at : "+b.getStageChars());
            System.exit(1);
        }

        long after = System.currentTimeMillis();

        System.out.println("\n《 BUILD SUCCESS, Executed in "+(after-before)/1000+"s 》");
        //kill any badly created plugin
        System.exit(0);
    }

    private static void printHelp() throws IOException{

        final CharOutputStream out = new CharOutputStream(new JVMOutputStream(System.out),CharEncodings.UTF_8);
        out.write(MODE_BOLD).write(new Chars("Ubuild help page")).write(MODE_RESET).endLine();
        out.write(new Chars("Ubuild is a pipeline style build tool, declarative working with an HJSON (https://hjson.org/) build file.")).write(MODE_RESET).endLine();
        out.endLine();
        out.write(MODE_BOLD).write(new Chars("Examples")).write(MODE_RESET).endLine();
        out.endLine();
        out.write(new Chars("Build using default build.hjson file : $> ubuild")).endLine();
        out.write(new Chars("Build using specified build file : $> ubuild -file myScript.hjson")).endLine();
        out.write(new Chars("Build starting from a given stage : $> ubuild -start step4.deploy.")).endLine();
        out.endLine();
        out.write(MODE_BOLD).write(new Chars("Parameters")).write(MODE_RESET).endLine();
        out.endLine();
        out.write(new Chars("-file")).endLine();
        out.write(new Chars("-start")).endLine();
        out.write(new Chars("-run")).endLine();
        out.write(new Chars("-p:xxx")).endLine();

    }

    private static Chars[] toChars(String[] args){
        final Chars[] chars = new Chars[args.length];
        for(int i=0;i<args.length;i++){
            chars[i] = new Chars(args[i]);
        }
        return chars;
    }

}
