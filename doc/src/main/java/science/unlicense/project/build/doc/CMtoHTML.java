
package science.unlicense.project.build.doc;

import science.unlicense.format.html.HTMLOutputStream;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;



/**
 *
 * @author Johann Sorel
 */
public class CMtoHTML extends science.unlicense.format.commonmark.CMtoHTML{

    private static final Chars NBSP2 = new Chars("&nbsp;&nbsp;");

    @Override
    protected void writeLine(HTMLOutputStream out, Chars text) throws IOException {
        if(text.startsWith('@')){
            text = text.truncate(1, -1);
            final int idx = text.getFirstOccurence(' ');
            if(idx>0){
                out.writeElementStart(null, new Chars("b"));
                out.writeText(text.truncate(0, idx));
                out.writeElementEnd(null, new Chars("b"));
                out.writeText(NBSP2);
                out.writeText(text.truncate(idx+1, -1));
                out.writeBr();
            }else{
                super.writeLine(out, text);
            }
        }else{
            super.writeLine(out, text);
        }
    }



}
