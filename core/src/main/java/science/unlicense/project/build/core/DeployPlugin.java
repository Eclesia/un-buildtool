
package science.unlicense.project.build.core;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 *
 * @author Johann Sorel
 */
public class DeployPlugin extends CommonPlugin{

    public static final Chars REPOSITORY_PATH = new Chars("repositoryPath");
    public static final Chars TARGET_PATH = new Chars("targetPath");

    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars atfPathStr = (Chars) params.getPropertyValue(TARGET_PATH);

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final ArtifactIdentifier[] dependencies = getDependencies(params);

        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        Path artifactPath = targetBasePath;
        if (atfPathStr != null) {
            artifactPath = targetBasePath.resolve(atfPathStr);
        }
        artifactPath = artifactPath.resolve(currentArtifactIdentifier.getArtifactName());

        DependencyResolvers.LOCAL_REPO.store(currentArtifactIdentifier, artifactPath, dependencies);
    }

}
