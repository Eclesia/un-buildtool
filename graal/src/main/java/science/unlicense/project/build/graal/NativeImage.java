
package science.unlicense.project.build.graal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.logging.DefaultLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommandOutputPrinter;
import science.unlicense.project.build.jdk.Tools;

/**
 *
 * @author Johann Sorel
 */
public class NativeImage {


    private Logger logger = new DefaultLogger();
    private Path graalHome;
    private Path mainClass;
    private Path[] classpathPaths;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Path getGraalHome() {
        return graalHome;
    }

    public void setGraalHome(Path graalHome) {
        this.graalHome = graalHome;
    }

    public Path getMainClass() {
        return mainClass;
    }

    public void setMainClass(Path mainClass) {
        this.mainClass = mainClass;
    }

    public Path[] getClasspathPaths() {
        return classpathPaths;
    }

    public void setClasspathPaths(Path[] classpathPaths) {
        this.classpathPaths = classpathPaths;
    }

    public void execute() throws Exception {

        final Path niPath = Tools.findTool(graalHome, new Chars("native-image"));

        final List<String> args = new ArrayList<>();
        args.add(Paths.systemPath(niPath).toString());

        final List<String> cps = new ArrayList<>();
        if (classpathPaths!=null) {
            for (int i=0;i<classpathPaths.length;i++) {
                listFilesStr(classpathPaths[i], new Chars(".jar"), cps);
                listFilesStr(classpathPaths[i], new Chars(".class"), cps);
            }
        }
        if (!cps.isEmpty()) {
            args.add("-cp");
            final StringBuilder sb = new StringBuilder();
            for (int i=0;i<cps.size();i++) {
                if(i!=0) sb.append(":");
                sb.append(cps.get(i));
            }
            args.add(sb.toString());
        }

        args.add("Runner");

        System.out.println(toShell());

        //graal native-image tool requiere to be run in folder containing main class
        final Path executionDir = mainClass.getParent();
        System.out.println(executionDir);

        final Process process = Runtime.getRuntime().exec(
                args.toArray(new String[0]),
                null,
                new File(Paths.systemPath(executionDir).toString()));
        new CommandOutputPrinter(process.getInputStream(),logger);
        new CommandOutputPrinter(process.getErrorStream(),logger);
        int res = process.waitFor();
        if (res!=0) {
            throw new Exception("Compilation fail");
        }

    }

    public Chars toShell() throws Exception {

        final Path niPath = Tools.findTool(graalHome, new Chars("native-image"));

        final List<String> args = new ArrayList<>();
        args.add(Paths.systemPath(niPath).toString());

        final List<String> cps = new ArrayList<>();
        if (classpathPaths!=null) {
            for (int i=0;i<classpathPaths.length;i++) {
                listFilesStr(classpathPaths[i], new Chars(".jar"), cps);
                listFilesStr(classpathPaths[i], new Chars(".class"), cps);
            }
        }
        if (!cps.isEmpty()) {
            args.add("-cp");
            final StringBuilder sb = new StringBuilder();
            for (int i=0;i<cps.size();i++) {
                if(i!=0) sb.append(":");
                sb.append(cps.get(i));
            }
            args.add(sb.toString());
        }

        args.add("Runner");

        final CharBuffer cb = new CharBuffer();
        cb.append(args.get(0));
        for (int i=1;i<args.size();i++) {
            cb.append(' ').append('"').append(args.get(i)).append('"');
        }

        return cb.toChars();
    }

    private static List<String> listFilesStr(Path root, Chars extension, List<String> sources) throws IOException {
        if (sources == null) {
            sources = new ArrayList<>();
        }

        if (root.isContainer()) {
            final Iterator ite = root.getChildren().createIterator();
            while (ite.hasNext()) {
                final Path child = (Path) ite.next();
                listFilesStr(child, extension, sources);
            }
        } else if (root.getName().endsWith(extension)) {
            sources.add(Paths.systemPath(root).toString());
        }

        return sources;
    }

}
