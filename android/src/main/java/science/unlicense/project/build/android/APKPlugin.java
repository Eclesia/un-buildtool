
package science.unlicense.project.build.android;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;

/**
 *
 * @author Johann Sorel
 */
public class APKPlugin extends CommonPlugin {

    public static final Chars ANDROID_SDK_HOME = new Chars("ANDROID_SDK_HOME");
    public static final Chars APPLICATION_NAME = new Chars("ApplicationName");

    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params, logger);

        final Path android_sdk_root = Paths.resolve(((Chars)params.getPropertyValue(ANDROID_SDK_HOME)));
        final Chars app_name = (Chars)params.getPropertyValue(APPLICATION_NAME);

        //search latest sdk version
        Chars version = null;
        final Iterator iteVersion = android_sdk_root.resolve(new Chars("build-tools")).getChildren().createIterator();
        while (iteVersion.hasNext()) {
            final Path p = (Path) iteVersion.next();
            if (p.isContainer()) {
                Chars name = p.getName();
                if(version==null || version.order(name)<0) version = name;
            }
        }
        final Path buildTools = android_sdk_root.resolve(new Chars("build-tools")).resolve(version);

        //search latest platform
        Chars platformVersion = null;
        final Iterator itePlatform = android_sdk_root.resolve(new Chars("platforms")).getChildren().createIterator();
        while (itePlatform.hasNext()) {
            final Path p = (Path) itePlatform.next();
            if (p.isContainer()) {
                Chars name = p.getName();
                if(platformVersion==null || platformVersion.order(name)<0) platformVersion = name;
            }
        }
        final Path platform = android_sdk_root.resolve(new Chars("platforms")).resolve(platformVersion);

        //tool paths
        final Path androidCp = platform.resolve(new Chars("android.jar"));
        final Path toolAapt = buildTools.resolve(new Chars("aapt"));
        final Path toolDx = buildTools.resolve(new Chars("dx"));
        final Path toolZipalign = buildTools.resolve(new Chars("zipalign"));
        final Path toolAdb = platform.resolve(new Chars("adb"));
        final Path toolJarsigner = platform.resolve(new Chars("jarsigner"));

        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);

        final String tempRes = "res";
        final String tempDir = "androidTemp";

        //use aapt tool
        final List<String> aaptArgs = new ArrayList<>();
        aaptArgs.add(Paths.systemPath(toolAapt).toString());
        aaptArgs.addAll(Arrays.asList("package","-f","-m","-J",tempDir,"-m","AndroidManifest.xml","-S",tempRes,"-I"));
        aaptArgs.add(Paths.systemPath(androidCp).toString());

        //TODO invoke javac or reuse previous build
        final String javaClasses = "todo";

        //use dx tool
        final List<String> dxArgs = new ArrayList<>();
        dxArgs.add(Paths.systemPath(toolDx).toString());
        dxArgs.addAll(Arrays.asList("--dex","--output=classes.dex",javaClasses));

        //use aapt tool
        final List<String> aapt2Args = new ArrayList<>();
        aapt2Args.add(Paths.systemPath(toolAapt).toString());
        aapt2Args.addAll(Arrays.asList("package","-f","-M","AndroidManifest.xml","-S",tempRes,"-I"));
        aapt2Args.add(Paths.systemPath(androidCp).toString());
        aapt2Args.addAll(Arrays.asList("-F",app_name.toString()+".apk.unaligned"));

        //use aapt tool
        final List<String> aapt3Args = new ArrayList<>();
        aapt3Args.add(Paths.systemPath(toolAapt).toString());
        aapt3Args.add("add");
        aapt3Args.add(app_name.toString()+".apk.unaligned");
        aapt3Args.add("classes.dex");

        //use jarsigner tool
        final String keyStorePath = "todo";
        final String keyStorePass = "todo";
        final String keyStoreKey = "todo";
        final List<String> jarSignArgs = new ArrayList<>();
        jarSignArgs.add(Paths.systemPath(toolJarsigner).toString());
        jarSignArgs.add("-keystore");
        jarSignArgs.add(keyStorePath);
        jarSignArgs.add("-storepass");
        jarSignArgs.add(keyStorePass);
        jarSignArgs.add(app_name.toString()+".apk.unaligned");
        jarSignArgs.add(keyStoreKey);

        //use zipalign tool
        final List<String> zipArgs = new ArrayList<>();
        zipArgs.add(Paths.systemPath(toolZipalign).toString());
        zipArgs.add("-f");
        zipArgs.add("4");
        zipArgs.add(app_name.toString()+".apk.unaligned");
        zipArgs.add(app_name.toString()+".apk");

        //use adb tool
        final List<String> adbArgs = new ArrayList<>();
        adbArgs.add(Paths.systemPath(toolAdb).toString());
        adbArgs.add("install");
        adbArgs.add("-r");
        adbArgs.add(app_name.toString()+".apk");
    }



}
