
package science.unlicense.project.build.core;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.Documents;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.impl.io.x364.X364;
import science.unlicense.format.json.HJSONReader;
import science.unlicense.format.json.JSONUtilities;

/**
 *
 * @author Johann Sorel
 */
public class Builder {

    public static final Chars ATT_CONDITION = new Chars("condition");
    public static final Chars ATT_PLUGIN = new Chars("plugin");
    public static final Chars ATT_SCRIPT = new Chars("script");
    public static final Chars ATT_EXECUTE = new Chars("execute");
    public static final Chars ATT_PARAMETERS = new Chars("parameters");
    public static final Chars ATT_TITLE = new Chars("title");
    public static final Chars ATT_MODULEID = new Chars("moduleId");
    public static final Chars ATT_REF = new Chars("$ref");

    private final Path configPath;
    private final Document config;
    private final Document globalParams;
    private TreeLogger logger = new TreeLogger();
    private Chars startFrom = null;
    private Chars[] run = null;
    //keep track of current stage we are running
    private Sequence stage = new ArraySequence();

    public Builder(Path configPath) throws IOException {
        this.configPath = configPath;
        this.config = openConfiguration(configPath);

        Document parameters = (Document) config.getPropertyValue(ATT_PARAMETERS);
        if (parameters == null) {
            parameters = new DefaultDocument(true);
        }
        globalParams = parameters;
    }

    public void setLogger(TreeLogger logger) {
        this.logger = logger;
    }

    public TreeLogger getLogger() {
        return logger;
    }

    public Sequence getStage() {
        return stage;
    }

    public void setStage(Sequence stage) {
        this.stage = stage;
    }

    public Chars getStageChars() {
        final CharBuffer buffer = new CharBuffer();
        for (int i=0,n=stage.getSize();i<n;i++) {
            if (i!=0) buffer.append('.');
            buffer.append(stage.get(i));
        }
        //last dot is important when testing equals
        buffer.append('.');
        return buffer.toChars();
    }

    public void setStartFrom(Chars startFrom) {
        this.startFrom = startFrom;
    }

    public Chars getStartFrom() {
        return startFrom;
    }

    public Chars[] getRun() {
        return run;
    }

    public void setRun(Chars[] run) {
        this.run = run;
    }

    public Document getGlobalParameters() {
        return globalParams;
    }

    public void execute() throws Exception {

        if (run == null || run.length == 0) {
            throw new Exception("Run condition not defined.");
        }

        //search for a script title
        Chars title = (Chars) globalParams.getPropertyValue(ATT_TITLE);
        if (title == null) {
            //fallback on module id if present
            title = (Chars) globalParams.getPropertyValue(ATT_MODULEID);
        }
        if (title == null) {
            //fallback on script path
            title = configPath.toURI();
        }

        logger.info(new Chars(""));
        logger.info(new Chars("⬤ "+X364.MODE_BOLD + title + X364.MODE_RESET));

        config.setPropertyValue(ATT_CONDITION, new Chars("build"));

        //TODO move this elsewhere ?
        //copy system properties in the global parameters
        final Properties systemProperties = System.getProperties();
        for (Entry<Object,Object> entry : systemProperties.entrySet()) {
            final Chars key = new Chars((String)entry.getKey());
            if (globalParams.getPropertyValue(key)==null) {
                globalParams.setPropertyValue(key, new Chars((String)entry.getValue()));
            }
        }
        final Map<String, String> envProperties = System.getenv();
        for (Entry<String,String> entry : envProperties.entrySet()) {
            final Chars key = new Chars((String)entry.getKey());
            if (globalParams.getPropertyValue(key)==null) {
                globalParams.setPropertyValue(key, new Chars(entry.getValue()));
            }
        }

        //load build steps
        logger.incrementDepth();
        final Document buildDoc = (Document) config.getPropertyValue(new Chars("build"));

        final Iterator stepNames = buildDoc.getPropertyNames().createIterator();
        while (stepNames.hasNext()) {
            final long bs = System.currentTimeMillis();

            //execute each step
            final Chars stepName = (Chars) stepNames.next();
            stage.add(stepName);

            Chars subRestart = null;
            if (startFrom != null) {
                if (!startFrom.startsWith(getStageChars())) {
                    stage.remove(stage.getSize()-1);
                    continue;
                }
                subRestart = startFrom.equals(getStageChars()) ? null : startFrom;
                startFrom = null;
            }

            final Document stepDoc = (Document) buildDoc.getPropertyValue(stepName);

            //check execution condition
            final Chars[] conditions = CommonPlugin.getValues(stepDoc, ATT_CONDITION);
            if (conditions != null && conditions.length > 0) {
                boolean exec = false;
                for (Chars cnd : run) {
                    if (Arrays.contains(conditions, cnd)) {
                        exec = true;
                        break;
                    }
                }
                if (!exec) {
                    stage.remove(stage.getSize()-1);
                    continue;
                }
            }

            final Chars plugin = (Chars) stepDoc.getPropertyValue(ATT_PLUGIN);
            final Object scripts = stepDoc.getPropertyValue(ATT_SCRIPT);
            final Chars execute = (Chars) stepDoc.getPropertyValue(ATT_EXECUTE);
            final Document stepParams = (Document) stepDoc.getPropertyValue(ATT_PARAMETERS);
            final Document allParams = new DefaultDocument();
            Documents.overrideFields(allParams, globalParams);
            if (stepParams != null) Documents.overrideFields(allParams, stepParams);
            final Path rootPath = configPath.getParent();
            allParams.setPropertyValue(new Chars("rootPath"), configPath.getParent());

            if (plugin != null) {
                //execute plugin
                final Class<?> pluginClass = Class.forName(plugin.toString());
                if (logger.getLogLevel() <= Logger.LEVEL_DEBUG) {
                    logger.info(new Chars("〇 "+X364.MODE_BOLD+stepName+X364.MODE_RESET+" : "+pluginClass.getSimpleName()+"."+execute +"   stage:["+getStageChars()+"]"));
                } else {
                    logger.info(new Chars("〇 "+X364.MODE_BOLD+stepName+X364.MODE_RESET+" : "+pluginClass.getSimpleName()+"."+execute));
                }

                logger.incrementDepth();
                final Object p = pluginClass.newInstance();
                final Method method = pluginClass.getMethod(execute.toString(), Document.class, Logger.class);
                method.invoke(p, allParams, logger);

                final long as = System.currentTimeMillis();
                logger.debug(new Chars("Executed in "+DecimalFormat.getInstance().format(((double)as-bs)/1000)+"s"));
                logger.decrementDepth();
            } else if (scripts != null) {
                //execute sub-scripts
                final Sequence elements;
                if (scripts instanceof Chars) {
                    elements = new ArraySequence();
                    elements.add(scripts);
                } else if(scripts instanceof Collection) {
                    elements = new ArraySequence((Collection) scripts);
                } else if(scripts instanceof Object[]) {
                    elements = new ArraySequence((Object[]) scripts);
                } else {
                    throw new Exception("Unvalid script value : "+scripts);
                }

                if (logger.getLogLevel() <= Logger.LEVEL_DEBUG) {
                    logger.info(new Chars("〇 "+X364.MODE_BOLD+stepName+X364.MODE_RESET+" : sub-scripts "+"   stage:["+getStageChars()+"]"));
                } else {
                    logger.info(new Chars("〇 "+X364.MODE_BOLD+stepName+X364.MODE_RESET+" : sub-scripts "));
                }
                logger.incrementDepth();

                for (int i=0,n=elements.getSize();i<n;i++) {
                    stage.add(Int32.encode(i));

                    Chars childRestart = null;
                    if (subRestart != null) {
                        if (!subRestart.startsWith(getStageChars())) {
                            stage.remove(stage.getSize()-1);
                            continue;
                        }
                        childRestart = subRestart.equals(getStageChars()) ? null : subRestart;
                        subRestart = null;
                    }

                    final Path p = rootPath.resolve((Chars)elements.get(i));
                    final Builder builder = new Builder(p);
                    builder.setLogger(this.logger);
                    builder.setStartFrom(childRestart);
                    builder.setStage(stage);
                    builder.setRun(run);
                    builder.execute();
                    stage.remove(stage.getSize()-1);
                }

                logger.decrementDepth();

            } else {
                logger.info(new Chars("No plugin or script specified for build step "+stepName));
            }

            stage.remove(stage.getSize()-1);
        }

        logger.decrementDepth();

    }

    private static Document openConfiguration(Path configPath) throws IOException {
        final HJSONReader reader = new HJSONReader();
        reader.setInput(configPath);
        final Document config = JSONUtilities.readAsDocument(reader, null);
        replaceReferences(configPath.getParent(), config,config);
        return config;
    }

    private static void replaceReferences(Path rootPath, Document root, Document doc) throws IOException {
        Object ref = doc.getPropertyValue(ATT_REF);
        if (ref!=null && ref instanceof Chars) {
            doc.setPropertyValue(ATT_REF, null);
            final Chars r = (Chars) ref;
            final Document link = (Document) getReference(rootPath, root, r);
            Documents.mergeDoc(link, doc);
            doc.set(link);
        }

        final Iterator names = doc.getPropertyNames().createIterator();
        while (names.hasNext()) {
            final Chars fieldName = (Chars)names.next();
            Object obj = doc.getPropertyValue(fieldName);
            if (obj instanceof Document) {
                replaceReferences(rootPath, root, (Document) obj);
            } else if (obj instanceof Chars) {
                Chars v = (Chars) obj;
                if(v.startsWith('#')) {
                    doc.setPropertyValue(fieldName,getReference(rootPath, root, v));
                }
            } else if (obj instanceof Object[]) {
                final Object[] array = (Object[]) obj;
                for (int i=0;i<array.length;i++) {
                    if(array[i] instanceof Document) {
                        replaceReferences(rootPath, root, (Document) array[i]);
                    }
                }
            }
        }

    }

    private static Object getReference(Path rootPath, Document root, Chars r) throws IOException {
        final Chars[] fileAndPath = r.split('#');
        Document base = root;
        if (!fileAndPath[0].isEmpty()) {
            base = openConfiguration(rootPath.resolve(fileAndPath[0]));
        }

        Document link = base;
        if (fileAndPath.length>1) {
            final Chars[] parts = fileAndPath[1].split('/');

            for (int i=0;i<parts.length;i++) {
                Object candidate = link.getPropertyValue(parts[i]);

                if (candidate==null) {
                    throw new IOException("Reference "+fileAndPath[1] +" is null.");
                } else if (candidate instanceof Document) {
                    link = (Document) candidate;
                } else if (i==parts.length-1) {
                    return candidate;
                } else {
                    throw new IOException("Reference "+fileAndPath[1] +" is not a document.");
                }
            }
        }
        return (Document) Documents.deepCopy(link);
    }

}
