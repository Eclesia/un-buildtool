
package science.unlicense.project.build.jdk;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 * Draft JLink plugin.
 *
 * JLink example :
 * create : $JDK_HOME/bin/jlink --module-path $JDK_HOME/jmods/java.base.jmod:$PROJECT_JARS --add-modules $MODULE_NAME1,$MODULE_NAME2 --output $OUTPUT_FOLDER
 * execute : ./bin/java -m $MODULE_NAME/$MODULE_MAIN_CLASS
 *
 * to create an executable :
 * create : $JDK_HOME/bin/jlink --module-path $JDK_HOME/jmods/java.base.jmod:$PROJECT_JARS --add-modules $MODULE_NAME1,$MODULE_NAME2 --output $OUTPUT_FOLDER --launcher myApp=$MODULE_NAME/$MODULE_MAIN_CLASS
 * execute : ./bin/myApp
 *
 * Documentation :
 * http://openjdk.java.net/projects/jigsaw/quick-start
 * https://dzone.com/articles/java-9-modular-development-part-2
 *
 * @author Johann Sorel
 */
public class JLinkPlugin extends CommonPlugin {

    public static final Chars TARGET_PATH = new Chars("targetPath");
    public static final Chars LAUNCHER = new Chars("launcher");
    public static final Chars MODULES = new Chars("modules");
    public static final Chars USE_WINE = new Chars("useWine");
    public static final Chars STRIP_DEBUG = new Chars("stripDebug");
    public static final Chars COMPRESS = new Chars("compress");


    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(TARGET_PATH);
        final boolean useWine = Boolean.TRUE.equals(params.getPropertyValue(USE_WINE));
        final boolean stripDebug = Boolean.TRUE.equals(params.getPropertyValue(STRIP_DEBUG));
        final Integer compress = ((Number) params.getPropertyValue(COMPRESS)).intValue();

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Path javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        final Path jmodsPath = javaHome.resolve(new Chars("jmods"));
        Chars[] modules = getValues(params, MODULES);
        final Chars launcher = (Chars) params.getPropertyValue(LAUNCHER);
        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        final Path targetPath = targetBasePath.resolve(tgtPathStr);

        //list all mod paths, this include jars
        final Chars[] repositories = getValues(params, REPOSITORIES);

        //resolve all dependencies
        final Sequence depPaths = new ArraySequence();
        depPaths.add(jmodsPath);
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);
        final Artifact[] artifacts = DependencyResolvers.importTransitive(new ArtifactIdentifier[]{currentArtifactIdentifier}, resolvers);
        for (Artifact art : artifacts) {
            depPaths.add(art.path);
        }


        //add dependencies as modules


        final Linker linker = new Linker();
        linker.setJavaHome(javaHome);
        linker.setLogger(logger);
        linker.setLauncher(launcher);
        linker.setModulePaths((Path[]) depPaths.toArray(Path.class));
        linker.setModules(modules);
        linker.setTargetPath(targetPath);
        linker.setUseWine(useWine);
        linker.setStripDebug(stripDebug);
        linker.setCompress(compress);
        linker.execute();

    }

}
