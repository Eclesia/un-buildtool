
package science.unlicense.project.build.teavm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 *
 * @author Johann Sorel
 */
public class TeavmPlugin extends CommonPlugin {

    public static final Chars GRAAL_HOME = new Chars("GRAAL_HOME");
    public static final Chars MAINCLASS = new Chars("mainClass");

    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Path graalHome = Paths.resolve(((Chars)params.getPropertyValue(GRAAL_HOME)));
        final Chars mainClass = (Chars) params.getPropertyValue(MAINCLASS);

        //list all mod paths, this include jars
        final Chars[] repositories = getValues(params, REPOSITORIES);

        //resolve all dependencies
        final Sequence depPaths = new ArraySequence();
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);
        final Artifact[] artifacts = DependencyResolvers.importTransitive(new ArtifactIdentifier[]{currentArtifactIdentifier}, resolvers);
        for (Artifact art : artifacts) {
            depPaths.add(art.path);
        }

//        final NativeImage ni = new NativeImage();
//        ni.setClasspathPaths((Path[]) depPaths.toArray(Path.class));
//        ni.setGraalHome(graalHome);
//        ni.setLogger(logger);
//        ni.setMainClass(rootPath.resolve(mainClass));
//        ni.execute();

    }
    
}
