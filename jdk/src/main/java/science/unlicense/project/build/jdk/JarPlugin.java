package science.unlicense.project.build.jdk;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommandOutputPrinter;
import science.unlicense.project.build.core.CommonPlugin;

/**
 *
 * @author Johann Sorel
 */
public class JarPlugin extends CommonPlugin {

    public static final Chars CONTENT_PATH = new Chars("contentPath");
    public static final Chars JAR_PATH = new Chars("jarPath");

    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        final Object pathStrs = params.getPropertyValue(CONTENT_PATH);
        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars jarPathStr = (Chars) params.getPropertyValue(JAR_PATH);

        final Path javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        final Path jartoolPath = Tools.findTool(javaHome, new Chars("jar"));

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Path targetPath = rootPath.resolve(tgtBasePathStr);

        //copy all content in the same place
        final Path regroupPath = targetPath.resolve(new Chars("jar"));
        regroupPath.createContainer();
        if (pathStrs instanceof Object[]) {
            for(Object c : (Object[])pathStrs) {
                Path p = rootPath.resolve((Chars)c);
                if(!p.exists()) continue;
                Paths.copy(p, regroupPath);
            }
        } else {
            Path p = rootPath.resolve((Chars)pathStrs);
            Paths.copy(p, regroupPath);
        }

        Path jarPath = targetPath;
        if (jarPathStr!=null) {
            jarPath = jarPath.resolve(jarPathStr);
        }
        jarPath = jarPath.resolve(currentArtifactIdentifier.getArtifactName());

        String compileParam = "cvf";
        //copy manifest informations if it exist
        Path manifest = regroupPath.resolve(new Chars("META-INF/MANIFEST.MF"));
        if (manifest.exists()) {
            compileParam += "m";
        }

        final List<String> args = new ArrayList<>();
        args.add(Paths.systemPath(jartoolPath).toString());
        args.add(compileParam);
        args.add(Paths.systemPath(jarPath).toString());
//
//        Iterator cite = contentPaths.createIterator();
//        while(cite.hasNext()) {
//            Path p = (Path) cite.next();
//            args.add(p.toURI().toString().replace("file:", ""));
//        }

        if (manifest.exists()) {
            args.add(Paths.systemPath(manifest).toString());
        }

        //change tool base path
        args.add("-C");
        args.add(Paths.systemPath(regroupPath).toString());
        args.add(".");

        if (true) {
            final CharBuffer sb2 = new CharBuffer();
            for(String s : args) {
                sb2.append(" "+s);
            }
            logger.debug(sb2.toChars());
        }

        final Process process = Runtime.getRuntime().exec(args.toArray(new String[0]));
        new CommandOutputPrinter(process.getInputStream(),logger);
        new CommandOutputPrinter(process.getErrorStream(),logger);
        int res = process.waitFor();
        if (res!=0) {
            throw new Exception("Jar creation fail");
        }

    }

}
