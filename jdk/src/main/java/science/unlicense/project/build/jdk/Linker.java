
package science.unlicense.project.build.jdk;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.DefaultLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommandOutputPrinter;

/**
 * JLink example :
 * create : $JDK_HOME/bin/jlink --module-path $JDK_HOME/jmods/java.base.jmod:$PROJECT_JARS --add-modules $MODULE_NAME1,$MODULE_NAME2 --output $OUTPUT_FOLDER
 * execute : ./bin/java -m $MODULE_NAME/$MODULE_MAIN_CLASS
 *
 * to create an executable :
 * create : $JDK_HOME/bin/jlink --module-path $JDK_HOME/jmods/java.base.jmod:$PROJECT_JARS --add-modules $MODULE_NAME1,$MODULE_NAME2 --output $OUTPUT_FOLDER --launcher myApp=$MODULE_NAME/$MODULE_MAIN_CLASS
 * execute : ./bin/myApp
 *
 * Documentation :
 * http://openjdk.java.net/projects/jigsaw/quick-start
 * https://dzone.com/articles/java-9-modular-development-part-2
 *
 * @author Johann Sorel
 */
public class Linker {

    private Logger logger = new DefaultLogger();
    private Path[] modulePaths;
    private Chars[] modules;
    private Chars launcher;
    private Path targetPath;
    private Path javaHome;
    private boolean useWine = false;
    private boolean stripDebug = false;
    private int compress = 0;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Path getJavaHome() {
        return javaHome;
    }

    public void setJavaHome(Path javaHome) {
        this.javaHome = javaHome;
    }

    public Path[] getModulePaths() {
        return modulePaths;
    }

    public void setModulePaths(Path[] modulePaths) {
        this.modulePaths = modulePaths;
    }

    public Chars[] getModules() {
        return modules;
    }

    public void setModules(Chars[] modules) {
        this.modules = modules;
    }

    public Chars getLauncher() {
        return launcher;
    }

    public void setLauncher(Chars launcher) {
        this.launcher = launcher;
    }

    public Path getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(Path targetPath) {
        this.targetPath = targetPath;
    }

    public void setUseWine(boolean useWine) {
        this.useWine = useWine;
    }

    public boolean isUseWine() {
        return useWine;
    }

    public void setStripDebug(boolean stripDebug) {
        this.stripDebug = stripDebug;
    }

    public boolean isStripDebug() {
        return stripDebug;
    }

    public void setCompress(int compress) {
        this.compress = compress;
    }

    public int getCompress() {
        return compress;
    }

    public void execute() throws Exception {
        targetPath.createContainer();
        link();
    }

    private void link() throws Exception {

        if (modulePaths==null || modulePaths.length==0) {
            logger.log(new Chars("Nothing to link."), Logger.LEVEL_INFORMATION);
            return;
        }

        final String separator = useWine ? ";" : ":";

        final Path jlinkPath = Tools.findTool(javaHome, new Chars("jlink"));

        final List<String> args = new ArrayList<>();
        if (useWine) args.add("wine");
        args.add(Paths.systemPath(jlinkPath).toString());

        //add module paths
        args.add("--module-path");
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<modulePaths.length;i++) {
            if(i!=0) sb.append(separator);
            if (useWine) {
                sb.append(Wine.getWinePath(modulePaths[i]).toString());
            } else {
                sb.append(Paths.systemPath(modulePaths[i]).toString());
            }
        }
        args.add(sb.toString());

        //add modules to include
        if(modules.length>0) {
            args.add("--add-modules");
            sb = new StringBuilder();
            for (int i=0;i<modules.length;i++) {
                if(i!=0) sb.append(",");
                sb.append(modules[i].toString());
            }
            args.add(sb.toString());
        }

        //add launcher
        if (launcher!=null && !launcher.isEmpty()) {
            args.add("--launcher");
            args.add(launcher.toString());
        }

        //add output
        args.add("--output");
        args.add(Paths.systemPath(targetPath).toString());
        targetPath.delete();

        // strip debug
        if (stripDebug) {
            args.add("--strip-debug");
        }

        // compress
        args.add("--compress=" + compress);

        if (true) {
            final CharBuffer sb2 = new CharBuffer();
            for(String s : args) {
                sb2.append(" "+s);
            }
            logger.debug(sb2.toChars());
        }

        //execute it
        final Process process = Runtime.getRuntime().exec(args.toArray(new String[0]));
        new CommandOutputPrinter(process.getInputStream(),logger);
        new CommandOutputPrinter(process.getErrorStream(),logger);
        int res = process.waitFor();
        if (res!=0) {
            throw new Exception("Linker failed");
        }
    }


}
