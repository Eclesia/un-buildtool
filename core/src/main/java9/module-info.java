module science.unlicense.project.build.core {
    requires java.compiler;
    requires science.unlicense.encoding;
    requires science.unlicense.system.jvm;
    requires science.unlicense.common;
    requires science.unlicense.binding.json;
    requires science.unlicense.binding.xml;
    requires science.unlicense.syntax;
    requires science.unlicense.system;
    requires science.unlicense.math;

    exports science.unlicense.project.build.core.dependency;
    exports science.unlicense.project.build.core;
}