
package science.unlicense.project.build.jdk;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommonPlugin;

/**
 * Draft JPackage plugin.
 *
 * JPackage example :
 * create : $JDK_HOME/bin/jpackage --name $NAME --module-path $PROJECT_JARS -m myapp
 * execute : ./bin/java -m $MODULE_NAME/$MODULE_MAIN_CLASS
 *
 * Documentation :
 * https://openjdk.java.net/jeps/343
 *
 * @author Johann Sorel
 */
public class JPackagePlugin extends CommonPlugin {

    public static final Chars TARGET_PATH = new Chars("targetPath");
    public static final Chars LAUNCHER = new Chars("launcher");
    public static final Chars MODULES = new Chars("modules");
    public static final Chars MODULE = new Chars("module");
    public static final Chars USE_WINE = new Chars("useWine");
    public static final Chars INPUT = new Chars("input");
    public static final Chars TYPE = new Chars("type");
    public static final Chars MAIN_CLASS = new Chars("mainClass");
    public static final Chars RUNTIME_IMAGE = new Chars("runtimeImage");


    @Override
    public void execute(Document params, Logger logger) throws Exception {
        super.execute(params,logger);

        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(TARGET_PATH);
        final Chars runtimePathStr = (Chars) params.getPropertyValue(RUNTIME_IMAGE);
        final boolean useWine = Boolean.TRUE.equals(params.getPropertyValue(USE_WINE));
        final Chars type = (Chars) params.getPropertyValue(TYPE);

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);
        final Path javaHome = Paths.resolve(((Chars)params.getPropertyValue(JAVA_HOME)));
        //final Path input = Paths.resolve(((Chars)params.getPropertyValue(INPUT)));
        final Path jmodsPath = javaHome.resolve(new Chars("jmods"));
        Chars[] modules = getValues(params, MODULES);
        final Chars launcher = (Chars) params.getPropertyValue(LAUNCHER);
        final Chars module = (Chars) params.getPropertyValue(MODULE);
        final Chars mainClass = (Chars) params.getPropertyValue(MAIN_CLASS);
        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        final Path targetPath = targetBasePath.resolve(tgtPathStr);
        final Path runtimeImage = targetBasePath.resolve(runtimePathStr);

        //list all mod paths, this include jars
        final Chars[] repositories = getValues(params, REPOSITORIES);

        //resolve all dependencies
//        final Sequence depPaths = new ArraySequence();
//        depPaths.add(jmodsPath);
//        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);
//        final Artifact[] artifacts = DependencyResolvers.importTransitive(new ArtifactIdentifier[]{currentArtifactIdentifier}, resolvers);
//        for (Artifact art : artifacts) {
//            depPaths.add(art.path);
//        }


        //add dependencies as modules


        final Packager packager = new Packager();
        packager.setJavaHome(javaHome);
        packager.setLogger(logger);
        packager.setType(type);
        packager.setRuntimeImage(runtimeImage);
        packager.setLauncher(launcher);
        packager.setModule(module);
        packager.setMainClass(mainClass);
//        packager.setModulePaths((Path[]) depPaths.toArray(Path.class));
//        packager.setModules(modules);
        //packager.setInput(input);
        packager.setDestPath(targetPath);
        packager.setUseWine(useWine);
        packager.execute();

    }

}
