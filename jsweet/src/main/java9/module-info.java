module science.unlicense.project.build.jsweet {
requires science.unlicense.encoding;
requires science.unlicense.system.jvm;
requires science.unlicense.common;
requires science.unlicense.binding.json;
requires science.unlicense.binding.xml;
requires science.unlicense.syntax;
requires science.unlicense.protocol.http;
requires science.unlicense.system;
requires science.unlicense.project.build.core;
requires science.unlicense.math;
requires jsweet.transpiler;
exports science.unlicense.project.build.jweet;
}