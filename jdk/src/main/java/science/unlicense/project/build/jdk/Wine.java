
package science.unlicense.project.build.jdk;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public final class Wine {

    public static Chars getWinePath(Path path) {

        String str = Paths.systemPath(path).toString();
        str = str.replace('/', '\\');
        str = "Z:"+str;
        return new Chars(str);

//        final Process process = Runtime.getRuntime().exec(new String[]{"winepath",path.toString()});
//        new CommandOutputPrinter(process.getInputStream());
//        new CommandOutputPrinter(process.getErrorStream());

    }

}
