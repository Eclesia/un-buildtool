
package science.unlicense.project.build.doc;

import java.util.Date;
import science.unlicense.code.api.CodeContext;
import science.unlicense.code.api.CodeFile;
import science.unlicense.code.api.CodeFileReader;
import science.unlicense.code.api.CodeProducer;
import science.unlicense.code.api.Parameter;
import science.unlicense.code.java.JavaFormat;
import science.unlicense.code.java.model.JavaClass;
import science.unlicense.code.java.model.JavaDocumentation;
import science.unlicense.code.java.model.JavaFunction;
import science.unlicense.code.java.model.JavaProperty;
import science.unlicense.code.java.model.JavaScope;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.DefaultLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.html.HTMLOutputStream;
import science.unlicense.format.xml.XMLOutputStream;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.xml.dom.DomNode;
import science.unlicense.format.xml.dom.DomReader;
import science.unlicense.format.xml.dom.DomUtilities;

/**
 *
 * @author Johann Sorel
 */
public class DocGenerator {

    private static final Chars STYLE_PROPERTY = new Chars("property");
    private static final Chars STYLE_COMMENT = new Chars("comment");
    private static final Chars TARGET_PAGE = new Chars("page");

    private final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
    private final CodeProducer ast2Code = JavaFormat.INSTANCE.createProducer();

    private final CodeContext cc = new CodeContext();

    //maven state
    private MavenMeta mavenMeta;

    private Logger logger = new DefaultLogger();
    private Sequence sourcePaths;
    private Path targetPath;
    private Chars title = Chars.EMPTY;
    private boolean maven = false;
    private Path image;

    public DocGenerator() {
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Sequence getSourcePaths() {
        return sourcePaths;
    }

    public void setSourcePaths(Sequence sourcePaths) {
        this.sourcePaths = sourcePaths;
    }

    public Path getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(Path targetPath) {
        this.targetPath = targetPath;
    }

    public Chars getTitle() {
        return title;
    }

    public void setTitle(Chars title) {
        this.title = title;
    }

    public boolean isMaven() {
        return maven;
    }

    public void setMaven(boolean maven) {
        this.maven = maven;
    }

    public void generate() throws Exception{
        for (int i=0,n=sourcePaths.getSize();i<n;i++) {
            test((Path) sourcePaths.get(i));
        }

        //copy resources
        final Path[] resources = new Path[]{
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/doc.css")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/CollapsibleLists.js")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/abstract.svg")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/interface.svg")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/class.svg")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/button.png")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/button-open.png")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/button-closed.png")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/tree-empty.png")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/tree-last.png")),
            Paths.resolve(new Chars("mod:/science/unlicense/project/build/doc/tree-middle.png"))
        };

        for(int i=0;i<resources.length;i++){
            final Path in = resources[i];
            final Path out = targetPath.resolve(resources[i].getName());
            IOUtilities.copy(in.createInputStream(), out.createOutputStream());
        }


        //generate index pages
        createHeadPage();
        createIndexPage();
        createSummaryListPage();
        createSummaryTreePage();
        createIntroductionPage();
        final Path indexPath = targetPath.resolve(new Chars("index.json"));
        CodeContextIO.write(indexPath, cc);
        CodeContextIO.read(indexPath, cc);

        //generate all clases page
        final Iterator ite = cc.elements.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Chars val1 = (Chars) pair.getValue1();
            final Object val2 = pair.getValue2();
            if (val2 instanceof JavaClass) {
                createClassPage((JavaClass) val2);
            }
        }
    }

    private void createIndexPage() throws IOException {

        final Path out = targetPath.resolve(new Chars("index.html"));

        final XMLOutputStream xs = new XMLOutputStream();
        xs.setEncoding(CharEncodings.UTF_8);
        xs.setIndent(new Chars("  "));
        xs.setOutput(out);

        xs.writeMetaStart(new Chars("DOCTYPE"));
        xs.writeText(new Chars("PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\""));
        xs.writeMetaEnd();
        xs.writeElementStart(null, new Chars("html"));

        xs.writeElementStart(null, new Chars("head"));
            xs.writeElementStart(null, new Chars("title"));
            xs.writeText(title);
            xs.writeElementEnd(null, new Chars("title"));
        xs.writeElementEnd(null, new Chars("head"));

        xs.writeElementStart(null, new Chars("frameset"));
        xs.writeProperty(new Chars("cols"), new Chars("340,*"));

            xs.writeElementStart(null, new Chars("frameset"));
            xs.writeProperty(new Chars("rows"), new Chars("120,*"));
                xs.writeElementStart(null, new Chars("frame"));
                xs.writeProperty(new Chars("frameborder"), new Chars("no"));
                xs.writeProperty(new Chars("name"), new Chars("head"));
                xs.writeProperty(new Chars("src"), new Chars("./head.html"));
                xs.writeElementEnd(null, new Chars("frame"));
                xs.writeElementStart(null, new Chars("frame"));
                xs.writeProperty(new Chars("frameborder"), new Chars("no"));
                xs.writeProperty(new Chars("name"), new Chars("index"));
                xs.writeProperty(new Chars("src"), new Chars("./summaryTree.html"));
                xs.writeElementEnd(null, new Chars("frame"));
            xs.writeElementEnd(null, new Chars("frameset"));


            xs.writeElementStart(null, new Chars("frame"));
            xs.writeProperty(new Chars("border"), new Chars("0"));
            xs.writeProperty(new Chars("name"), new Chars("page"));
            xs.writeProperty(new Chars("src"), new Chars("./introduction.html"));
            xs.writeElementEnd(null, new Chars("frame"));
        xs.writeElementEnd(null, new Chars("frameset"));

        xs.writeElementEnd(null, new Chars("html"));
        xs.dispose();
    }

    private void createHeadPage() throws IOException {

        if (image != null) {
            final Path out = targetPath.resolve(new Chars("logo.png"));
            IOUtilities.copy(image.createInputStream(), out.createOutputStream());
        }

        final Path out = targetPath.resolve(new Chars("head.html"));

        final HTMLOutputStream xs = new HTMLOutputStream();
        xs.setEncoding(CharEncodings.UTF_8);
        xs.setIndent(new Chars("  "));
        xs.setOutput(out);

        xs.writeElementStart(null, new Chars("html"));
        xs.writeElementStart(null, new Chars("head"));
            xs.writeElementStart(null, new Chars("link"));
            xs.writeProperty(new Chars("rel"), new Chars("stylesheet"));
            xs.writeProperty(new Chars("type"), new Chars("text/css"));
            xs.writeProperty(new Chars("href"), new Chars("./doc.css"));
            xs.writeElementEnd(null, new Chars("link"));
        xs.writeElementEnd(null, new Chars("head"));

        xs.writeElementStart(null, new Chars("body"));


        xs.writeElementStart(null, new Chars("center"));
            xs.writeElementStart(null, new Chars("h2"));
            xs.writeElementStart(null, new Chars("img"));
            xs.writeProperty(new Chars("src"), new Chars("./logo.png"));
            xs.writeProperty(new Chars("height"), new Chars("28"));
            xs.writeProperty(new Chars("align"), new Chars("top"));
            xs.writeElementEnd(null, new Chars("img"));
            xs.writeText(title);
            xs.writeElementEnd(null, new Chars("h2"));
        xs.writeElementEnd(null, new Chars("center"));
        //xs.writeBr();

        xs.writeElementStart(null, new Chars("center"));
        xs.writeElementStart(null, new Chars("h3"));
        xs.writeAText(new Chars("LIST"), new Chars("summaryList.html"), new Chars("index"));
        xs.writeAText(new Chars("TREE"), new Chars("summaryTree.html"), new Chars("index"));
        xs.writeElementEnd(null, new Chars("h3"));
        xs.writeElementEnd(null, new Chars("center"));

        xs.writeElementEnd(null, new Chars("body"));
        xs.writeElementEnd(null, new Chars("html"));

        xs.dispose();
    }

    private void createIntroductionPage() throws IOException {

        if (image != null) {
            final Path out = targetPath.resolve(new Chars("logo.png"));
            IOUtilities.copy(image.createInputStream(), out.createOutputStream());
        }

        final Path out = targetPath.resolve(new Chars("introduction.html"));

        final HTMLOutputStream xs = new HTMLOutputStream();
        xs.setEncoding(CharEncodings.UTF_8);
        xs.setIndent(new Chars("  "));
        xs.setOutput(out);

        xs.writeElementStart(null, new Chars("html"));
        xs.writeElementStart(null, new Chars("head"));
            xs.writeElementStart(null, new Chars("link"));
            xs.writeProperty(new Chars("rel"), new Chars("stylesheet"));
            xs.writeProperty(new Chars("type"), new Chars("text/css"));
            xs.writeProperty(new Chars("href"), new Chars("./doc.css"));
            xs.writeElementEnd(null, new Chars("link"));
        xs.writeElementEnd(null, new Chars("head"));


        xs.writeElementStart(null, new Chars("body"));


        xs.writeElementStart(null, new Chars("center"));
            xs.writeElementStart(null, new Chars("img"));
            xs.writeProperty(new Chars("src"), new Chars("./logo.png"));
            xs.writeElementEnd(null, new Chars("img"));
            xs.writeBr();
            xs.writeElementStart(null, new Chars("h2"));
            xs.writeText(title);
            xs.writeElementEnd(null, new Chars("h2"));
            xs.writeText(new Chars(new Date().toString()));
        xs.writeElementEnd(null, new Chars("center"));
        xs.writeBr();

        xs.writeElementEnd(null, new Chars("body"));
        xs.writeElementEnd(null, new Chars("html"));

        xs.dispose();
    }

    private void createSummaryListPage() throws IOException {

        final Path out = targetPath.resolve(new Chars("summaryList.html"));

        final HTMLOutputStream xs = new HTMLOutputStream();
        xs.setEncoding(CharEncodings.UTF_8);
        xs.setIndent(new Chars("  "));
        xs.setOutput(out);

        xs.writeElementStart(null, new Chars("html"));
        xs.writeElementStart(null, new Chars("head"));
            xs.writeElementStart(null, new Chars("link"));
            xs.writeProperty(new Chars("rel"), new Chars("stylesheet"));
            xs.writeProperty(new Chars("type"), new Chars("text/css"));
            xs.writeProperty(new Chars("href"), new Chars("./doc.css"));
            xs.writeElementEnd(null, new Chars("link"));
        xs.writeElementEnd(null, new Chars("head"));

        xs.writeElementStart(null, new Chars("body"));


        final Sequence seq = new ArraySequence(cc.elements.getKeys());
        Collections.sort(seq);
        for (int i = 0,n = seq.getSize(); i < n; i++) {
            final Chars val1 = (Chars) seq.get(i);
            final Object val2 = cc.elements.getValue(val1);

            if (val2 instanceof JavaClass) {
                xs.writeAText(val1, new Chars("./classes/"+val1+".html"), TARGET_PAGE);
                xs.writeBr();
            }
        }

        xs.writeElementEnd(null, new Chars("body"));
        xs.writeElementEnd(null, new Chars("html"));

        xs.dispose();
    }

    private void createSummaryTreePage() throws IOException {

        final Path out = targetPath.resolve(new Chars("summaryTree.html"));

        final HTMLOutputStream xs = new HTMLOutputStream();
        xs.setEncoding(CharEncodings.UTF_8);
        xs.setIndent(new Chars("  "));
        xs.setOutput(out);

        xs.writeElementStart(null, new Chars("html"));
        xs.writeElementStart(null, new Chars("head"));
            xs.writeElementStart(null, new Chars("link"));
            xs.writeProperty(new Chars("rel"), new Chars("stylesheet"));
            xs.writeProperty(new Chars("type"), new Chars("text/css"));
            xs.writeProperty(new Chars("href"), new Chars("./doc.css"));
            xs.writeElementEnd(null, new Chars("link"));
            xs.setAutoclosing(false);
            xs.writeElementStart(null, new Chars("script"));
            xs.writeProperty(new Chars("type"), new Chars("text/javascript"));
            xs.writeProperty(new Chars("src"), new Chars("CollapsibleLists.js"));
            xs.writeElementEnd(null, new Chars("script"));
            xs.setAutoclosing(true);
        xs.writeElementEnd(null, new Chars("head"));

        xs.writeElementStart(null, new Chars("body"));

        //sort by packages
        final Dictionary sort = new OrderedHashDictionary();
        final Sequence seq = new ArraySequence(cc.elements.getKeys());
        Collections.sort(seq);
        for (int i = 0, n = seq.getSize(); i < n;i++) {
            final Chars val1 = (Chars) seq.get(i);
            final Object val2 = cc.elements.getValue(val1);

            final Chars[] path = val1.split('.');
            Dictionary base = sort;
            for (int k = 0;k < path.length-1; k++) {
                Object d = base.getValue(path[k]);
                if (d == null) {
                    d = new OrderedHashDictionary();
                    base.add(path[k],d);
                }
                base = (Dictionary) d;
            }

            if (val2 instanceof JavaClass) {
                base.add(path[path.length-1], val2);
            }
        }

        //generate tree
        xs.writeElementStart(null, new Chars("ul"));
        xs.writeProperty(new Chars("class"), new Chars("treeView"));
        final Iterator ite = sort.getPairs().createIterator();
        int k = 0;
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            writeTreeNode(xs, (Chars) pair.getValue1(), pair.getValue2(), k == (sort.getSize()-1),true);
            k++;
        }
        xs.writeElementEnd(null, new Chars("ul"));


        xs.writeElementEnd(null, new Chars("body"));

        xs.setAutoclosing(false);
        xs.writeElementStart(null, new Chars("script"));
        xs.writeText(new Chars("CollapsibleLists.apply();"));
        xs.writeElementEnd(null, new Chars("script"));
        xs.setAutoclosing(true);

        xs.writeElementEnd(null, new Chars("html"));

        xs.dispose();
    }

    private static void writeTreeNode(final HTMLOutputStream xs, Chars name, Object value, boolean last, boolean root) throws IOException {
        if (value instanceof Dictionary) {
            xs.writeElementStart(null, new Chars("li"));
            if (!root) {
                xs.writeProperty(new Chars("class"), last ? new Chars("lastChild collapsibleListOpen") : new Chars("collapsibleListOpen"));
            } else {
                xs.writeProperty(new Chars("class"), new Chars(""));
            }
            xs.writeText(name);

            xs.writeElementStart(null, new Chars("ul"));
            xs.writeProperty(new Chars("class"), new Chars("collapsibleList"));
            xs.writeProperty(new Chars("style"), new Chars("display: block;"));
            final Dictionary d = (Dictionary) value;
            final Iterator ite = d.getPairs().createIterator();
            int k = 0;
            while (ite.hasNext()) {
                final Pair pair = (Pair) ite.next();
                writeTreeNode(xs, (Chars) pair.getValue1(), pair.getValue2(),k==(d.getSize()-1),false);
                k++;
            }
            xs.writeElementEnd(null, new Chars("ul"));

            xs.writeElementEnd(null, new Chars("li"));
        } else {
            final JavaClass clazz = (JavaClass) value;

            xs.writeElementStart(null, new Chars("li"));
            xs.writeProperty(new Chars("class"), last ? new Chars("lastChild leaf") : new Chars("leaf"));

            xs.writeElementStart(null, new Chars("a"));
            if(clazz.isInterface()){
                xs.writeProperty(new Chars("class"), new Chars("interface"));
            }else if(clazz.isAbstract()){
                xs.writeProperty(new Chars("class"), new Chars("abstract"));
            }else{
                xs.writeProperty(new Chars("class"), new Chars("class"));
            }
            xs.writeProperty(new Chars("href"), new Chars("./classes/"+clazz.getPackage()+"."+clazz.getShortName()+".html"));
            xs.writeProperty(new Chars("target"),TARGET_PAGE);
            xs.writeText(clazz.getShortName());
            xs.writeElementEnd(null, new Chars("a"));

            xs.writeElementEnd(null, new Chars("li"));
        }
    }

    private void createClassPage(JavaClass clazz) throws IOException {

        final Path parent = targetPath.resolve(new Chars("classes"));
        parent.createContainer();
        final Path out = parent.resolve(clazz.id.concat(new Chars(".html")));
        out.createLeaf();

        final HTMLOutputStream xs = new HTMLOutputStream();
        xs.setEncoding(CharEncodings.UTF_8);
        xs.setIndent(new Chars("  "));
        xs.setOutput(out);

        xs.writeElementStart(null, new Chars("html"));
        xs.writeElementStart(null, new Chars("head"));
            xs.writeElementStart(null, new Chars("link"));
            xs.writeProperty(new Chars("rel"), new Chars("stylesheet"));
            xs.writeProperty(new Chars("type"), new Chars("text/css"));
            xs.writeProperty(new Chars("href"), new Chars("../doc.css"));
            xs.writeElementEnd(null, new Chars("link"));
        xs.writeElementEnd(null, new Chars("head"));

        xs.writeElementStart(null, new Chars("body"));

        if (maven) {
            final MavenMeta mm = (MavenMeta) clazz.metas.get(MavenMeta.ID);
            if (mm != null) {
                xs.writeSpanText(new Chars("Maven module :"), STYLE_PROPERTY);
                xs.writeText(new Chars(mm.groupId+" : "+mm.artifactId+" : "+ mm.version));
                xs.writeBr();
            }
        }
        xs.writeSpanText(new Chars("Class :"), STYLE_PROPERTY);
        xs.writeElementStart(null, new Chars("b"));
            xs.writeText(clazz.id);
        xs.writeElementEnd(null, new Chars("b"));
        xs.writeBr();

        //TODO need to resolve references
        xs.writeSpanText(new Chars("Extends/Implements :"), STYLE_PROPERTY);
        if (clazz.parents.isEmpty()) {
            xs.writeText(new Chars(" - "));
        } else {
            for (int i = 0, n = clazz.parents.getSize(); i < n; i++) {
                final Chars pid = (Chars) clazz.parents.get(i);
                xs.writeText(new Chars(" "));
                xs.writeAText(pid, new Chars("./"+pid+".html"), TARGET_PAGE);
            }
        }
        xs.writeBr();
        xs.writeSpanText(new Chars("Subclasses :"), STYLE_PROPERTY);
        //TODO
        xs.writeText(new Chars(" - "));
        xs.writeBr();

        writeDoc(xs, clazz.getDocumentation());

        // LIST OF PROPERTIES AND FUNCTIONS ////////////////////////////////////
        xs.writeBr();
        xs.writeHr();

        //get all accessible properties
        xs.writeSpanText(new Chars("Variables :"), STYLE_PROPERTY);
        final Sequence properties = listProperties(clazz);
        if (properties.isEmpty()) {
            xs.writeText(new Chars(" - "));
        } else {
            for (int i = 0, n = properties.getSize(); i < n; i++) {
                final JavaProperty fct = (JavaProperty) properties.get(i);
                if (i == 0) xs.writeText(new Chars(" "));
                if (i != 0) xs.writeText(new Chars(", "));
                xs.writeAText(fct.id, new Chars("#prop_"+fct.id),null);
            }
        }
        xs.writeBr();

        //get all accessible functions
        xs.writeSpanText(new Chars("Functions :"), STYLE_PROPERTY);
        final Sequence functions = listFunctions(clazz);
        if (functions.isEmpty()) {
            xs.writeText(new Chars(" - "));
        } else {
            for (int i = 0, n = functions.getSize(); i < n; i++) {
                final JavaFunction fct = (JavaFunction) functions.get(i);
                if(i==0) xs.writeText(new Chars(" "));
                if(i!=0) xs.writeText(new Chars(", "));
                xs.writeAText(fct.id, new Chars("#fct_"+fct.id),null);
            }
        }
        xs.writeBr();

        // DETAILED PROPERTIES /////////////////////////////////////////////////
        xs.writeHr();

        for (int i = 0, n = properties.getSize(); i < n; i++) {
            xs.writeBr();
            final JavaProperty prop = (JavaProperty) properties.get(i);
            writeDoc(xs, (JavaDocumentation) prop.metas.get(JavaDocumentation.ID));

            //scope
            final JavaScope scope = (JavaScope) prop.metas.get(JavaScope.ID);
            if (JavaScope.PUBLIC == scope) {
                xs.writeText(new Chars("public "));
            } else if(JavaScope.PROTECTED == scope) {
                xs.writeText(new Chars("protected "));
            }

            //out parameters
            xs.writeText(prop.objclass.id);

            //name
            xs.writeAnchor(prop.id, new Chars("prop_"+prop.id));

            xs.writeBr();
        }

        // DETAILED FUNCTIONS //////////////////////////////////////////////////
        xs.writeHr();

        for (int i = 0, n = functions.getSize(); i < n; i++) {
            xs.writeBr();
            final JavaFunction fct = (JavaFunction) functions.get(i);
            writeDoc(xs, (JavaDocumentation) fct.metas.get(JavaDocumentation.ID));

            //scope
            final JavaScope scope = (JavaScope) fct.metas.get(JavaScope.ID);
            if(JavaScope.PUBLIC == scope){
                xs.writeText(new Chars("public "));
            } else if (JavaScope.PROTECTED == scope) {
                xs.writeText(new Chars("protected "));
            }

            //out parameters
            if (fct.outParameters.isEmpty()) {
                xs.writeText(new Chars("void "));
            } else {
                for (int k = 0,kn = fct.outParameters.getSize(); k < kn; k++) {
                    final Parameter param = (Parameter) fct.outParameters.get(k);
                    if (k != 0) {
                        xs.writeText(new Chars(", "));
                    }
                    xs.writeText(param.type.id);
                }
            }

            //name
            xs.writeAnchor(fct.id, new Chars("fct_"+fct.id));

            //in parameters
            xs.writeText(new Chars(" ("));
            for (int k = 0, kn = fct.inParameters.getSize(); k < kn; k++) {
                final Parameter param = (Parameter) fct.inParameters.get(k);
                if (k != 0) {
                    xs.writeText(new Chars(", "));
                }
                xs.writeText(param.type.id);
                xs.writeText(new Chars(" "));
                xs.writeText(param.id);
            }
            xs.writeText(new Chars(")"));
            xs.writeBr();
        }

        xs.writeElementEnd(null, new Chars("body"));
        xs.writeElementEnd(null, new Chars("html"));

        xs.dispose();
    }

    private void writeDoc(HTMLOutputStream xs, JavaDocumentation doc) throws IOException {
        if (doc != null) {
            xs.writeBr();
            xs.writeElementStart(null, new Chars("div"));
            xs.writeProperty(new Chars("class"), STYLE_COMMENT);

            //convert markdown
            try {
                final Chars md = new CMtoHTML().writeToChars(doc.getText());
                xs.writeText(md);
            } catch (Throwable ex) {
                logger.warning(new Chars(ex.getMessage()));
                CharArray[] parts = doc.getText().split('\n');
                for (int i = 0; i < parts.length; i++) {
                    xs.writeText(parts[i]);
                    xs.writeBr();
                }
            }


            xs.writeElementEnd(null, new Chars("div"));
        }
    }

    private Sequence listFunctions(JavaClass clazz) {
        final Sequence lst = new ArraySequence();
        for (int i = 0, n = clazz.functions.getSize(); i < n; i++) {
            final JavaFunction fct = (JavaFunction) clazz.functions.get(i);
            if (!JavaScope.PRIVATE.equals(fct.metas.get(JavaScope.ID))) {
                lst.add(fct);
            }
        }
        return lst;
    }

    private Sequence listProperties(JavaClass clazz) {
        final Sequence lst = new ArraySequence();
        for (int i = 0, n = clazz.properties.getSize(); i < n; i++) {
            final JavaProperty fct = (JavaProperty) clazz.properties.get(i);
            if (!JavaScope.PRIVATE.equals(fct.metas.get(JavaScope.ID))) {
                lst.add(fct);
            }
        }
        return lst;
    }

    private void test(Path path) throws Exception {
        Chars name = path.getName();
        if (path.isContainer()) {

            final Path[] children = (Path[]) path.getChildren().toArray(Path.class);
            for (Path n : children) {
                test(n);
            }
        } else {
            if (name.endsWith(new Chars(".java"))) {
                toDoc(path);
            }
        }
    }

    private void readMavenPom(Path p) throws IOException {

        final DomReader reader = new DomReader();
        reader.setInput(p);
        final DomNode doc = reader.read();

        //find groupid, artifactid and version
        final DomElement groupNode = DomUtilities.getNodeForName(doc, new Chars("groupId"));
        final DomElement artifactNode = DomUtilities.getNodeForName(doc, new Chars("artifactId"));
        final DomElement versionNode = DomUtilities.getNodeForName(doc, new Chars("version"));
        mavenMeta = new MavenMeta(
                groupNode.getText(),
                artifactNode.getText(),
                versionNode == null ? new Chars("") : versionNode.getText());


    }

    private void toDoc(Path p) throws Exception {

        long before = System.currentTimeMillis();

        reader.setInput(p);
        try {
            final CodeFile ast = reader.read();
            CodeContext cc = ast2Code.createCodeContext(ast);

            if (maven && mavenMeta != null) {
                //add maven meta on all classes
                final Iterator ite = cc.elements.getValues().createIterator();
                while (ite.hasNext()) {
                    final Object obj = ite.next();
                    if (obj instanceof science.unlicense.code.api.Class) {
                        ((science.unlicense.code.api.Class) obj).metas.add(mavenMeta);
                    }
                }
            }

            this.cc.elements.addAll(cc.elements);
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            long after = System.currentTimeMillis();
            logger.debug(new Chars((after - before) + "ms\t\t" + p));
        }
    }

}
