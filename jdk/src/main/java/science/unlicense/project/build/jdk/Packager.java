
package science.unlicense.project.build.jdk;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.DefaultLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.project.build.core.CommandOutputPrinter;

/**
 * JPacakge
 *
 * Documentation :
 * https://openjdk.java.net/jeps/343
 *
 * @author Johann Sorel
 */
public class Packager {

    private Logger logger = new DefaultLogger();
    private Path[] modulePaths;
    private Chars[] modules;
    private Chars launcher;
    private Chars module;
    private Path destPath;
    private Path javaHome;
    private Path input;
    private Path runtimeImage;
    private Chars name;
    private Chars type;
    private Chars mainClass;
    private boolean useWine = false;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Path getJavaHome() {
        return javaHome;
    }

    public void setJavaHome(Path javaHome) {
        this.javaHome = javaHome;
    }

    public Path getRuntimeImage() {
        return runtimeImage;
    }

    public void setRuntimeImage(Path runtimeImage) {
        this.runtimeImage = runtimeImage;
    }

    public Chars getType() {
        return type;
    }

    public void setType(Chars type) {
        this.type = type;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public Chars getMainClass() {
        return mainClass;
    }

    public void setMainClass(Chars mainClass) {
        this.mainClass = mainClass;
    }

    public Path[] getModulePaths() {
        return modulePaths;
    }

    public void setModulePaths(Path[] modulePaths) {
        this.modulePaths = modulePaths;
    }

    public Chars[] getModules() {
        return modules;
    }

    public void setModules(Chars[] modules) {
        this.modules = modules;
    }

    public Chars getLauncher() {
        return launcher;
    }

    public void setLauncher(Chars launcher) {
        this.launcher = launcher;
    }

    public Chars getModule() {
        return module;
    }

    public void setModule(Chars module) {
        this.module = module;
    }

    public Path getDestPath() {
        return destPath;
    }

    public void setDestPath(Path targetPath) {
        this.destPath = targetPath;
    }

    public Path getInput() {
        return input;
    }

    public void setInput(Path input) {
        this.input = input;
    }

    public void setUseWine(boolean useWine) {
        this.useWine = useWine;
    }

    public boolean isUseWine() {
        return useWine;
    }

    public void execute() throws Exception {
        destPath.createContainer();
        pack();
    }

    private void pack() throws Exception {

//        if (modulePaths == null || modulePaths.length == 0) {
//            logger.log(new Chars("Nothing to package."), Logger.LEVEL_INFORMATION);
//            return;
//        }

        final String separator = useWine ? ";" : ":";

        final Path jpackagePath = Tools.findTool(javaHome, new Chars("jpackage"));

        final List<String> args = new ArrayList<>();
        if (useWine) args.add("wine");
        args.add(Paths.systemPath(jpackagePath).toString());

        //add name
        if (name != null && !name.isEmpty()) {
            args.add("--name");
            args.add(name.toString());
        }

        //add application image
        if (runtimeImage != null) {
            args.add("--runtime-image");
            args.add(Paths.systemPath(runtimeImage).toString());
        }

        //add input
        if (input != null) {
            args.add("--input");
            args.add(Paths.systemPath(input).toString());
        }

        //add output
        args.add("--dest");
        args.add(Paths.systemPath(destPath).toString());
        destPath.delete();

        //add module paths
        if (modulePaths != null) {
            args.add("--module-path");
            StringBuilder sb = new StringBuilder();
            for (int i=0;i<modulePaths.length;i++) {
                if(i!=0) sb.append(separator);
                if (useWine) {
                    sb.append(Wine.getWinePath(modulePaths[i]).toString());
                } else {
                    sb.append(Paths.systemPath(modulePaths[i]).toString());
                }
            }
            args.add(sb.toString());
        }

        //add modules to include
        if (modules != null) {
            if (modules.length > 0) {
                args.add("--add-modules");
                StringBuilder sb = new StringBuilder();
                for (int i=0;i<modules.length;i++) {
                    if (i!=0) sb.append(",");
                    sb.append(modules[i].toString());
                }
                args.add(sb.toString());
            }
        }

        //add launcher
        if (launcher != null && !launcher.isEmpty()) {
            args.add("--add-launcher");
            args.add(launcher.toString());
        }

        //add module
        if (module != null && !module.isEmpty()) {
            args.add("--module");
            args.add(module.toString());
        }

        //add main class
        if (mainClass != null && !mainClass.isEmpty()) {
            args.add("--main-class");
            args.add(mainClass.toString());
        }

        //add type
        if (type != null) {
            args.add("--type");
            args.add(type.toString());
        }

        if (true) {
            final CharBuffer sb2 = new CharBuffer();
            for(String s : args) {
                sb2.append(" "+s);
            }
            logger.debug(sb2.toChars());
        }

        //execute it
        final Process process = Runtime.getRuntime().exec(args.toArray(new String[0]));
        new CommandOutputPrinter(process.getInputStream(),logger);
        new CommandOutputPrinter(process.getErrorStream(),logger);
        int res = process.waitFor();
        if (res != 0) {
            throw new Exception("Packager failed");
        }
    }


}
