
package science.unlicense.project.build.doc;

import science.unlicense.archive.api.Archive;
import science.unlicense.archive.api.Archives;
import science.unlicense.code.api.CodeContext;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.VirtualPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.zip.ZipWriter;
import science.unlicense.project.build.core.CommonPlugin;
import static science.unlicense.project.build.core.CommonPlugin.DEPENDENCIES;
import static science.unlicense.project.build.core.CommonPlugin.ROOT_PATH;
import static science.unlicense.project.build.core.CommonPlugin.TARGETBASE_PATH;
import science.unlicense.project.build.core.dependency.Artifact;
import science.unlicense.project.build.core.dependency.ArtifactIdentifier;
import science.unlicense.project.build.core.dependency.DependencyResolver;
import science.unlicense.project.build.core.dependency.DependencyResolvers;

/**
 *
 * @author Johann Sorel
 */
public class DocPlugin extends CommonPlugin {

    private static final Chars DOCTYPE = new Chars(".doc.zip");

    public static final Chars PROPERTY_SOURCEPATH = new Chars("sourcePath");
    public static final Chars PROPERTY_TARGETPATH = new Chars("targetPath");
    public static final Chars PROPERTY_LANGUAGE = new Chars("language");
    public static final Chars PROPERTY_MAVEN = new Chars("maven");
    public static final Chars PROPERTY_TITLE = new Chars("title");
    public static final Chars PROPERTY_IMAGE = new Chars("image");

    public static final Chars SOURCE_PATH = new Chars("sourcePath");
    public static final Chars TARGET_PATH = new Chars("targetPath");

    /**
     * Generate documentation for a single module.
     *
     * @param params
     * @param logger
     * @throws Exception
     */
    @Override
    public void execute(Document params, Logger logger) throws Exception {
        params.setPropertyValue(TYPE, DOCTYPE);
        super.execute(params, logger);

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);

        final Object pathStrs = params.getPropertyValue(SOURCE_PATH);
        final Chars tgtBasePathStr = (Chars) params.getPropertyValue(TARGETBASE_PATH);
        final Chars tgtPathStr = (Chars) params.getPropertyValue(TARGET_PATH);
        final Path targetBasePath = rootPath.resolve(tgtBasePathStr);
        final Path targetPath = targetBasePath.resolve(tgtPathStr);
        final Path archivePath = targetBasePath.resolve(currentArtifactIdentifier.getArtifactName());

        final Sequence sourcePaths = new ArraySequence();
        if (pathStrs instanceof Object[]) {
            for(Object c : (Object[])pathStrs) {
                sourcePaths.add(rootPath.resolve((Chars)c));
            }
        } else {
            sourcePaths.add(rootPath.resolve((Chars)pathStrs));
        }
        targetPath.createContainer();

        final DocGenerator gen = new DocGenerator();
        gen.setLogger(logger);
        gen.setTitle((Chars) params.getPropertyValue(PROPERTY_TITLE));
        gen.setSourcePaths(sourcePaths);
        gen.setTargetPath(targetPath);

        try {
            gen.generate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final ZipWriter writer = new ZipWriter();
        writer.setOutput(archivePath);
        writer.write(targetPath);
        writer.dispose();
    }

    /**
     * Aggregate documentation of multiple modules.
     *
     * @param params
     * @param logger
     * @throws Exception
     */
    public void aggregate(Document params, Logger logger) throws Exception {
        params.setPropertyValue(TYPE, DOCTYPE);
        super.execute(params, logger);

        final Path rootPath = (Path) params.getPropertyValue(ROOT_PATH);

        final Chars[] repositories = getValues(params, REPOSITORIES);
        final ArtifactIdentifier[] directDependencies = getDependencies(params, DEPENDENCIES);
        final DependencyResolver[] resolvers = DependencyResolvers.getResolvers(repositories);

        //resolve all dependencies
        final VirtualPath.Ram vp = new VirtualPath.Ram(Chars.EMPTY);

        final CodeContext cc = new CodeContext();
        final VirtualPath.Ram classes = (VirtualPath.Ram) vp.resolve(new Chars("classes"));
        classes.createContainer();

        final Sequence allDependencies = new ArraySequence(DependencyResolvers.importTransitive(directDependencies, resolvers));
        final Iterator ite = allDependencies.createIterator();
        while (ite.hasNext()) {
            Artifact artifact = (Artifact) ite.next();
            artifact = artifact.sibling(DOCTYPE);
            if (artifact.path.exists()) {
                final Archive archive = Archives.open(artifact.path);
                final Path index = archive.resolve(new Chars("index.json"));
                CodeContextIO.read(index, cc);

                final Path classesSub = archive.resolve(new Chars("classes"));

                final Iterator classIte = classesSub.getChildren().createIterator();
                while (classIte.hasNext()) {
                    final Path classPath = (Path) classIte.next();
                    final VirtualPath vp2 = VirtualPath.decorate(classPath, null);
                    classes.addChildren(0, new Path[]{vp2});
                }
            }
        }

        final ZipWriter writer = new ZipWriter();
        Path path = rootPath.resolve(currentArtifactIdentifier.getArtifactName());
        System.out.println(path.toURI());
        writer.setOutput(path);
        writer.write(vp);
        writer.dispose();


        logger.critical(new Chars("number" + allDependencies.getSize()));

    }
}
