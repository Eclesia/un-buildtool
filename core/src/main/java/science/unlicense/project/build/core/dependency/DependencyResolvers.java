
package science.unlicense.project.build.core.dependency;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class DependencyResolvers {

    public static final DependencyResolver LOCAL_REPO = new FolderDependencyResolver(
            Paths.resolve(new Chars(System.getProperty("user.home"))).resolve(new Chars(".ubt")));

    public static DependencyResolver[] getResolvers(Chars[] adresses) {
        final DependencyResolver[] resolvers = new DependencyResolver[adresses.length];
        for (int i=0;i<adresses.length;i++) {
            resolvers[i] = new MavenDependencyResolver(Paths.resolve(adresses[i]));
        }
        return resolvers;
    }

    /**
     * Import given dependencies in local repository.
     * This include all transitive dependencies.
     *
     * @param dependencies
     * @return Path array to all artifacts including transitive ones
     */
    public static Artifact[] importTransitive(ArtifactIdentifier[] dependencies, DependencyResolver[] resolvers) throws IOException {
        final Set artifacts = new HashSet();
        for (ArtifactIdentifier dep : dependencies) {
            importTransitive(artifacts, dep, resolvers);
        }
        return (Artifact[]) artifacts.toArray(Artifact.class);
    }

    private static void importTransitive(Set artifacts, ArtifactIdentifier dependency, DependencyResolver[] resolvers) throws IOException {
        Path path = DependencyResolvers.LOCAL_REPO.find(dependency);
        if (path == null) {
            //seach repositories
            for (DependencyResolver r : resolvers) {
                path = r.find(dependency);
                if (path!=null) {
                    System.out.println("Found : "+path.toURI());
                    //copy dependency in local repository, get the local path
                    final ArtifactIdentifier[] trss = r.listTransitive(dependency);
                    path = DependencyResolvers.LOCAL_REPO.store(dependency, path, trss);
                    break;
                }
            }
        }
        if (path == null) {
            throw new IOException("Dependency "+dependency+" not found in any repository");
        }

        artifacts.add(new Artifact(dependency,path));

        //add transitives
        final ArtifactIdentifier[] transitives = DependencyResolvers.LOCAL_REPO.listTransitive(dependency);
        for (ArtifactIdentifier trs : transitives) {
            importTransitive(artifacts, trs, resolvers);
        }
    }

}
