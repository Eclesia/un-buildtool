
package science.unlicense.project.build.test;

/**
 *
 * @author Johann Sorel
 */
public final class Assert {

    private Assert() {

    }

    public static void assertTrue(boolean value) {
        if (!value) {
            throw new AssertException(true,value);
        }
    }

    public static void assertFalse(boolean value) {
        if (value) {
            throw new AssertException(false,value);
        }
    }

    public static void assertEquals(boolean expected, boolean value) {
        if (expected != value) {
            throw new AssertException(expected,value);
        }
    }

    public static void assertEquals(long expected, long value) {
        if (expected != value) {
            throw new AssertException(expected,value);
        }
    }

    public static void assertEquals(double expected, double value) {
        if (expected != value) {
            throw new AssertException(expected,value);
        }
    }

    public static void assertEquals(double expected, double value, double tolerance) {
        if (expected != value) {
            if (Math.abs(expected-value) > tolerance ) {
                throw new AssertException(expected,value);
            }
        }
    }

    public static void assertEquals(Object expected, Object value) {
        if (expected == null) {
            if (value != null) {
                throw new AssertException(expected,value);
            }
        } else if (!expected.equals(value)) {
            throw new AssertException(expected,value);
        }
    }

    public static void assertArrayEquals(byte[] expected, byte[] value) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(short[] expected, short[] value) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(int[] expected, int[] value) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(long[] expected, long[] value) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(float[] expected, float[] value) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(float[] expected, float[] value, float tolerance) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                if (Math.abs(expected[i]-value[i]) > tolerance ) {
                    throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
                }
            }
        }
    }

    public static void assertArrayEquals(double[] expected, double[] value) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(double[] expected, double[] value, double tolerance) {
        if (expected.length != value.length) throw new AssertException("Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                if (Math.abs(expected[i]-value[i]) > tolerance ) {
                    throw new AssertException("Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
                }
            }
        }
    }

    public static void fail(){
        throw new AssertException();
    }

    public static void fail(String message){
        throw new AssertException(message);
    }

    public static void fail(String message, Throwable cause){
        throw new AssertException(message,cause);
    }

}
