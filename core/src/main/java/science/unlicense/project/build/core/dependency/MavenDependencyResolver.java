
package science.unlicense.project.build.core.dependency;

import java.net.HttpURLConnection;
import java.net.URL;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.xml.dom.DomNode;
import science.unlicense.format.xml.dom.DomReader;
import science.unlicense.format.xml.dom.DomUtilities;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.system.jvm.JVMInputStream;

/**
 *
 * @author Johann Sorel
 */
public class MavenDependencyResolver implements DependencyResolver {

    private final Path base;

    public MavenDependencyResolver(Path base) {
        this.base = base;
    }

    @Override
    public Path find(ArtifactIdentifier dependency) throws IOException {
        final Chars artifactName = dependency.getArtifactName();

        //for maven, type == classifier
        Chars classifier = dependency.type;

        Path artifactPath = base;
        for (Chars part : dependency.groupId.split('.')) {
            artifactPath = artifactPath.resolve(part);
        }
        artifactPath = artifactPath.resolve(dependency.moduleId);
        artifactPath = artifactPath.resolve(dependency.version);
        Path candidate = artifactPath.resolve(artifactName);
        if (candidate.exists()) {
            return candidate;
        }

        //special case for SNAPSHOT versions
        snapsearch:
        if (dependency.version.getFirstOccurence(new Chars("SNAPSHOT")) >= 0) {
            //search metadata file
            final Path metaPath = artifactPath.resolve(new Chars("maven-metadata.xml"));
            if (metaPath.exists()) {

                //hack against artifactory tool not returning the complete metadata xml for obscure and illogic reasons.
                ByteInputStream in = null;
                try {
                    if (metaPath.toURI().toString().startsWith("http")) {
                        final URL url = new URL(metaPath.toURI().toString());
                        final HttpURLConnection cnx = (HttpURLConnection) url.openConnection();
                        cnx.setRequestProperty("User-Agent", "Artifactory is trash.");
                        in = new JVMInputStream(cnx.getInputStream());
                    } else {
                        in = metaPath.createInputStream();
                    }

                    final Path path = findSnapshotPath(artifactPath, dependency, in);
                    if (path != null) {
                        return path;
                    }
                } catch (Exception ex) {
                    throw new IOException(ex.getMessage(), ex);
                }
            }
        }

        return null;
    }

    @Override
    public Path store(ArtifactIdentifier dependency, Path artifact, ArtifactIdentifier[] transitive) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public ArtifactIdentifier[] listTransitive(ArtifactIdentifier dependency) throws IOException {
        //TODO
        return new ArtifactIdentifier[0];
    }

    private static Path findSnapshotPath(Path artifactPath, ArtifactIdentifier dependency, ByteInputStream in) throws IOException {
        final DomReader reader = new DomReader();
        reader.setInput(in);
        final DomNode node = reader.read();
        reader.dispose();

        final DomElement versioning = DomUtilities.getNodeForName(node, new Chars("versioning"));
        if (versioning==null) return null;
        final DomElement snapshotVersions = DomUtilities.getNodeForName(versioning, new Chars("snapshotVersions"));
        if (snapshotVersions==null) return null;

        //search for version with qualifier jar and extension jar
        final Iterator ite = snapshotVersions.getChildren().createIterator();
        while (ite.hasNext()) {
            final DomElement vers = (DomElement) ite.next();
            final DomElement classi     = DomUtilities.getNodeForName(vers, new Chars("classifier"));
            final DomElement value      = DomUtilities.getNodeForName(vers, new Chars("value"));
            final DomElement extension  = DomUtilities.getNodeForName(vers, new Chars("extension"));

            if (classi!=null) {
                if (new Chars("jar-with-dependencies").equals(classi.getText())) {
                    final Chars text = value.getText();
                    final Chars snapVer = new Chars(dependency.moduleId+"-"+text+".jar");
                    final Path candidate = artifactPath.resolve(snapVer);
                    if (candidate.exists()) {
                        return candidate;
                    }
                }
            }
        }
        return null;
    }
}
