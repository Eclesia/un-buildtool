
package science.unlicense.project.build.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;

/**
 *
 * @author Johann Sorel
 */
public class CommandOutputPrinter implements Runnable {

    private Logger logger;
    private final BufferedReader br;

    public CommandOutputPrinter(InputStream is, Logger logger) {
        this.logger = logger;
        this.br = new BufferedReader(new InputStreamReader(is));
        new Thread(this).start();
    }

    @Override
    public void run() {
        try {
            for (String l=br.readLine();l!=null;l=br.readLine()) {
                logger.info(new Chars(l));
            }
        } catch (IOException ex) {
            logger.critical(ex);
        }
    }
}
