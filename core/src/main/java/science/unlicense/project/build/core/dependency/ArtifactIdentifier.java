
package science.unlicense.project.build.core.dependency;

import java.util.Objects;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class ArtifactIdentifier extends CObject{

    public final Chars groupId;
    public final Chars moduleId;
    public final Chars version;
    public final Chars type;

    public ArtifactIdentifier(Chars groupId, Chars moduleId, Chars version, Chars type) {
        this.groupId = groupId;
        this.moduleId = moduleId;
        this.version = version;
        this.type = type;
    }

    public Chars getArtifactName() {
        return new Chars(moduleId+"-"+version+type);
    }

    @Override
    public Chars toChars() {
        return new CharBuffer().append(groupId).append(':').append(moduleId).append(':').append(version).append(':').append(type).toChars();
    }

    @Override
    public int getHash() {
        return groupId.getHash() + moduleId.getHash() + version.getHash() + type.getHash();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArtifactIdentifier other = (ArtifactIdentifier) obj;
        if (!Objects.equals(this.groupId, other.groupId)) {
            return false;
        }
        if (!Objects.equals(this.moduleId, other.moduleId)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

}
